# ts-react-scheduler

#### Project Start: 2020-09-23

<hr/>

![example-pic](http://www.schlender-entwicklung.com/github-pics/ts-react-scheduler.png)

<hr/>

[Look and Feel](http://www.schlender-entwicklung.com/storybooks/ts-components-test/?path=/story/scheduler-1-0-9--index)

<hr/>

### Abstract:

Here you will find a ReactJS component with which you can display and organize your appointments in a web project.
You can easily transfer an array of events to the component and the events are visualized.
How you can do this is explained below.

<hr/>

### Documented Version:

v1.0.11

<hr/>

### Features:

- Design based on Material-UI
- Four date modes (day, week, month, year)
- Togglable header elements
- More than 70 properties to help you with an individual design
- Functions as properties to help with the further use of the data
- There is also an opportunity to display events as read-only
- There is a dialog for configuring / exploring current event
- Event dialog contains a color picker
- Long lasting events can be marked
- No required properties - you can start right away
- Labels are configurable, which helps with translation
- TypeScript implementation
- ...

<hr/>

## Installation

```
npm install --save ts-react-scheduler
```

<hr/>

## Usage

```
import {
    Scheduler
} from 'ts-react-scheduler';

...

<Scheduler />

```

<hr/>

## CSS

./node_modules/ts-react-scheduler/dist/index.css;

If you want to use the material style for your basic elements, then we recommend the following library

https://www.npmjs.com/package/fontsource-roboto

<hr/>

## Run Storybook:

```sh
npm run storybook
```

- open storybook: http://localhost:6006

<hr/>

## Generate Static Storybook:

```sh
npm run build-storybook
```

- result: ./storybook-static/index.html

<hr/>

## Properties

| Property                | Type     | Required | Default               | Info                                                                                                                                             |
| ----------------------- | -------- | -------- | --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| contentHeight           | number   | false    | 300                   |                                                                                                                                                  |
| dayNameSun              | string   | false    | Sunday                | can be used for translation                                                                                                                      |
| dayNameMon              | string   | false    | Monday                | can be used for translation                                                                                                                      |
| dayNameTue              | string   | false    | Tuesday               | can be used for translation                                                                                                                      |
| dayNameWed              | string   | false    | Wednesday             | can be used for translation                                                                                                                      |
| dayNameThu              | string   | false    | Thursday              | can be used for translation                                                                                                                      |
| dayNameFri              | string   | false    | Friday                | can be used for translation                                                                                                                      |
| dayNameSat              | string   | false    | Saturday              | can be used for translation                                                                                                                      |
| events                  | array    | false    | Default Demo Events   |                                                                                                                                                  |
| eventDialogTitleCreate  | string   | false    | Create Event          | can be used for translation                                                                                                                      |
| eventDialogTitleUpdate  | string   | false    | Update Event          | can be used for translation                                                                                                                      |
| headerDateFormat        | string   | false    | YYYY-MM-DD            |                                                                                                                                                  |
| headerTitle             | string   | false    | Tsdev React Scheduler | can be used for translation                                                                                                                      |
| id                      | string   | false    | TsdevReactScheduler   |                                                                                                                                                  |
| labelDialogBtnCancel    | string   | false    | Cancel                | can be used for translation                                                                                                                      |
| labelDialogBtnCreate    | string   | false    | Create                | can be used for translation                                                                                                                      |
| labelDialogBtnDelete    | string   | false    | Delete                | can be used for translation                                                                                                                      |
| labelDialogBtnUpdate    | string   | false    | Update                | can be used for translation                                                                                                                      |
| labelHeaderBtnDay       | string   | false    | Day                   | can be used for translation                                                                                                                      |
| labelHeaderBtnMonth     | string   | false    | Month                 | can be used for translation                                                                                                                      |
| labelHeaderBtnToday     | string   | false    | Today                 | can be used for translation                                                                                                                      |
| labelHeaderBtnWeek      | string   | false    | Week                  | can be used for translation                                                                                                                      |
| labelHeaderBtnYear      | string   | false    | Year                  | can be used for translation                                                                                                                      |
| labelDialogColor        | string   | false    | Color                 | can be used for translation                                                                                                                      |
| labelDialogDescription  | string   | false    | Description           | can be used for translation                                                                                                                      |
| labelDialogEnd          | string   | false    | End                   | can be used for translation                                                                                                                      |
| labelDialogLongLasting  | string   | false    | Long Lasting          | can be used for translation                                                                                                                      |
| labelDialogTitle        | string   | false    | Title                 | can be used for translation                                                                                                                      |
| labelDialogUntil        | string   | false    | Until                 | can be used for translation                                                                                                                      |
| labelDialogUntilDaily   | string   | false    | Mark as daily event   | can be used for translation                                                                                                                      |
| labelDialogUntilWeekly  | string   | false    | Mark as weekly event  | can be used for translation                                                                                                                      |
| labelDialogUntilMonthly | string   | false    | Mark as monthly event | can be used for translation                                                                                                                      |
| labelDialogUntilYearly  | string   | false    | Mark as yearly event  | can be used for translation                                                                                                                      |
| monthNameJan            | string   | false    | January               | can be used for translation                                                                                                                      |
| monthNameFeb            | string   | false    | February              | can be used for translation                                                                                                                      |
| monthNameMar            | string   | false    | March                 | can be used for translation                                                                                                                      |
| monthNameApr            | string   | false    | April                 | can be used for translation                                                                                                                      |
| monthNameMay            | string   | false    | May                   | can be used for translation                                                                                                                      |
| monthNameJun            | string   | false    | June                  | can be used for translation                                                                                                                      |
| monthNameJul            | string   | false    | July                  | can be used for translation                                                                                                                      |
| monthNameAug            | string   | false    | August                | can be used for translation                                                                                                                      |
| monthNameSep            | string   | false    | September             | can be used for translation                                                                                                                      |
| monthNameOct            | string   | false    | October               | can be used for translation                                                                                                                      |
| monthNameNov            | string   | false    | Novemver              | can be used for translation                                                                                                                      |
| monthNameDec            | string   | false    | December              | can be used for translation                                                                                                                      |
| noDataText              | string   | false    | No data               | can be used for translation                                                                                                                      |
| onClickBtnDay           | function | false    | noop                  | first argument is an object with keys: selectedYear (number), selectedMonth (number), selectedDay (number), eventsForDay (array)                 |
| onClickBtnMonth         | function | false    | noop                  | first argument is an object with keys: selectedYear (number), selectedMonth (number), selectedDay (number), eventsForMonth (array)               |
| onClickBtnNext          | function | false    | noop                  | first argument is an object with keys: selectedYear (number), selectedMonth (number), selectedDay (number)                                       |
| onClickBtnPrev          | function | false    | noop                  | first argument is an object with keys: selectedYear (number), selectedMonth (number), selectedDay (number)                                       |
| onClickBtnWeek          | function | false    | noop                  | first argument is an object with keys: selectedYear (number), selectedMonth (number), selectedDay (number), eventsForWeek (array), week (number) |
| onClickBtnYear          | function | false    | noop                  | first argument is an object with keys: selectedYear (number), selectedMonth (number), selectedDay (number), eventsForYear (array), week (number) |
| onCloseEventDialog      | function | false    | noop                  | first argument is an object with keys: selectedYear (number), selectedMonth (number), selectedDay (number)                                       |
| onCreateEvent           | function | false    | noop                  | first argument is an object with keys: newEvent (object)                                                                                         |
| onDeleteEvent           | function | false    | noop                  | first argument is an object with keys: deletedEvent (object), filtred events (array)                                                             |
| onOpenEventDialog       | function | false    | noop                  | first argument is an object with keys: selectedYear (number), selectedMonth (number), selectedDay (number)                                       |
| onUpdateEvent           | function | false    | noop                  | first argument is an object with keys: updatedEvent (object)                                                                                     |
| readOnly                | boolean  | false    | false                 |                                                                                                                                                  |
| selectedYear            | number   | false    | Current year          |                                                                                                                                                  |
| selectedMonth           | number   | false    | Current month         |                                                                                                                                                  |
| selectedDay             | number   | false    | Current day           |                                                                                                                                                  |
| showHeader              | boolean  | false    | true                  |                                                                                                                                                  |
| showHeaderAction        | boolean  | false    | true                  |                                                                                                                                                  |
| showHeaderBtnGroupDay   | boolean  | false    | true                  |                                                                                                                                                  |
| showHeaderBtnGroupWeek  | boolean  | false    | true                  |                                                                                                                                                  |
| showHeaderBtnGroupMonth | boolean  | false    | true                  |                                                                                                                                                  |
| showHeaderBtnGroupYear  | boolean  | false    | true                  |                                                                                                                                                  |
| showHeaderDate          | boolean  | false    | true                  |                                                                                                                                                  |
| showHeaderLongLasting   | boolean  | false    | true                  |                                                                                                                                                  |
| showHeaderNavigation    | boolean  | false    | true                  |                                                                                                                                                  |
| showHeaderTitle         | boolean  | false    | true                  |                                                                                                                                                  |
| showNoDataAlert         | boolean  | false    | true                  |                                                                                                                                                  |
| subClass                | string   | false    | ''                    |                                                                                                                                                  |
| tooltipHeaderBtnNext    | string   | false    | Next                  | can be used for translation                                                                                                                      |
| tooltipHeaderBtnPrev    | string   | false    | Previous              | can be used for translation                                                                                                                      |

<hr/>

### Events:

The events are processed as an array of individual event items.
An event item consists of several properties.

#### Event Item:

| Property               | Type    | Required |
| ---------------------- | ------- | -------- |
| color                  | string  | yes      |
| description            | string  | yes      |
| id                     | string  | yes      |
| endYear                | number  | yes      |
| endMonth               | number  | yes      |
| endDay                 | number  | yes      |
| endHour                | number  | yes      |
| endMinute              | number  | yes      |
| isLongLastingDayItem   | boolean | yes      |
| isLongLastingWeekItem  | boolean | yes      |
| isLongLastingMonthItem | boolean | yes      |
| isLongLastingYearItem  | boolean | yes      |
| startYear              | number  | yes      |
| startMonth             | number  | yes      |
| startDay               | number  | yes      |
| startHour              | number  | yes      |
| startMinute            | number  | yes      |
| untilYear              | number  | yes      |
| untilMonth             | number  | yes      |
| untilDay               | number  | yes      |

<br/>

#### Example:

```
import {
    Scheduler
} from 'ts-react-scheduler';

// ...

const myEvents = [];
const mySpecialEvent = {
    title: 'This is my title',
    description: 'This is only a demo description',
    color: '#64b5f6',
    startYear: 2020,
    startMonth: 8,
    startDay: 1,
    startHour: 3,
    startMinute: 45,
    endYear: 2020,
    endMonth: 8,
    endDay: 1,
    endHour: 4,
    endMinute: 45,
    untilYear: 2021,
    untilMonth: 8,
    untilDay: 1,
    isLongLastingDayItem: true,
    isLongLastingWeekItem: false,
    isLongLastingMonthItem: false,
    isLongLastingYearItem: true,
};

myEvents.push(mySpecialEvent);

// ...

<Scheduler events={myEvents}/>
```

<hr/>

## How to ...

### Create a new event:

Make sure the property for **readOnly** is set to **false**.
Click the header button for **Day**.
Then you can double-click on a corresponding time slot for when you are planning the event.
A dialog opens where you can make settings.
If the event slot (besides the time slot) is still free, you can double click here as well.

### Update an event:

Make sure the property for **readOnly** is set to **false**.
Click the header button for **Day**.
Then you can click on a corresponding event item.
A dialog opens where you can make settings.
It is also possible to click on a long scaled event in the header.

<hr/>
