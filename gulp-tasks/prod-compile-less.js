/* eslint-disable @typescript-eslint/no-var-requires */
const autoPrefixer = require('gulp-autoprefixer');
const less = require('gulp-less');

module.exports = function (gulp) {
  return function () {
    return gulp
      .src('./src/index.less')
      .pipe(less())
      .pipe(
        autoPrefixer({
          cascade: false,
        }),
      )
      .pipe(gulp.dest('./dist'));
  };
};
