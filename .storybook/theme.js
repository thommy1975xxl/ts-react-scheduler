import { create } from '@storybook/theming/create';

export default create({
    brandTitle: 'React Scheduler',
});