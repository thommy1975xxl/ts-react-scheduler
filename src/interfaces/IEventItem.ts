export interface IEventItem {
  id: string;
  title: string;
  description: string;
  color: string;
  startYear: number;
  startMonth: number;
  startDay: number;
  startHour: number;
  startMinute: number;
  endYear: number;
  endMonth: number;
  endDay: number;
  endHour: number;
  endMinute: number;
  untilYear: number;
  untilMonth: number;
  untilDay: number;
  isLongLastingDayItem: boolean;
  isLongLastingWeekItem: boolean;
  isLongLastingMonthItem: boolean;
  isLongLastingYearItem: boolean;
}
