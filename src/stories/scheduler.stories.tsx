import React from 'react';
import { Story } from '@storybook/react/types-6-0';
import { Scheduler } from './../';

const Template: Story = (args) => <Scheduler {...args} />;

export default {
  title: 'Scheduler',
  component: Scheduler,
  parameters: { actions: { argTypesRegex: '^on.*' } },
};

export const Index = Template.bind({});

Index.args = {};
