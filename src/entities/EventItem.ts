import { IEventItem } from '../interfaces/IEventItem';

export class EventItem implements IEventItem {
  id: string;
  title: string;
  description: string;
  color: string;
  startYear: number;
  startMonth: number;
  startDay: number;
  startHour: number;
  startMinute: number;
  endYear: number;
  endMonth: number;
  endDay: number;
  endHour: number;
  endMinute: number;
  untilYear: number;
  untilMonth: number;
  untilDay: number;
  isLongLastingDayItem: boolean;
  isLongLastingWeekItem: boolean;
  isLongLastingMonthItem: boolean;
  isLongLastingYearItem: boolean;

  constructor(
    id: string,
    title: string,
    description: string,
    color: string,
    startYear: number,
    startMonth: number,
    startDay: number,
    startHour: number,
    startMinute: number,
    endYear: number,
    endMonth: number,
    endDay: number,
    endHour: number,
    endMinute: number,
    untilYear: number,
    untilMonth: number,
    untilDay: number,
    isLongLastingDayItem: boolean,
    isLongLastingWeekItem: boolean,
    isLongLastingMonthItem: boolean,
    isLongLastingYearItem: boolean,
  ) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.color = color;
    this.startYear = startYear;
    this.startMonth = startMonth;
    this.startDay = startDay;
    this.startHour = startHour;
    this.startMinute = startMinute;
    this.endYear = endYear;
    this.endMonth = endMonth;
    this.endDay = endDay;
    this.endHour = endHour;
    this.endMinute = endMinute;
    this.untilYear = untilYear;
    this.untilMonth = untilMonth;
    this.untilDay = untilDay;
    this.isLongLastingDayItem = isLongLastingDayItem;
    this.isLongLastingWeekItem = isLongLastingWeekItem;
    this.isLongLastingMonthItem = isLongLastingMonthItem;
    this.isLongLastingYearItem = isLongLastingYearItem;
  }
}
