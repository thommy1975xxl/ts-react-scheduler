import React from 'react';
import TestRenderer from 'react-test-renderer';
import { ContentScale } from '../../components/content/ContentScale';

describe('components | content | ContentScale.tsx', () => {
  const component = <ContentScale />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });
});
