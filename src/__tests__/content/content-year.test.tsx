import React from 'react';
import TestRenderer from 'react-test-renderer';
import { ContentYear } from '../../components/content/ContentYear';
import { mount } from 'enzyme';

describe('components | content | ContentYear.tsx', () => {
  const component = <ContentYear />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });

  test('Should change year', () => {
    const changeYear = jest.fn();
    const wrapper = mount(component);
    const submitButton = wrapper.find('.year-item').at(0);

    submitButton.simulate('click');

    expect(changeYear).toHaveBeenCalled;
  });
});
