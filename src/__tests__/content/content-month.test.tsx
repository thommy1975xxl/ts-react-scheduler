import React from 'react';
import TestRenderer from 'react-test-renderer';
import { ContentMonth } from '../../components/content/ContentMonth';
import { mount } from 'enzyme';

describe('components | content | ContentMonth.tsx', () => {
  const component = <ContentMonth />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });

  test('Should change day', () => {
    const changeDay = jest.fn();
    const wrapper = mount(component);
    const submitButton = wrapper.find('.month-item').at(0);

    submitButton.simulate('click');

    expect(changeDay).toHaveBeenCalled;
  });
});
