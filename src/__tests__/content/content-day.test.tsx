import React from 'react';
import TestRenderer from 'react-test-renderer';
import { ContentDay } from '../../components/content/ContentDay';
import { mount } from 'enzyme';

describe('components | content | ContentDay.tsx', () => {
  const component = <ContentDay />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });

  test('Should ensure double click for time slot', () => {
    const openDialogModeCreate = jest.fn();
    const wrapper = mount(component);
    const submitButton = wrapper.find('.time-slot').at(0);

    submitButton.simulate('dblclick');

    expect(openDialogModeCreate).toHaveBeenCalled;
  });

  test('Should ensure double click for event slot', () => {
    const openDialogModeCreate = jest.fn();
    const wrapper = mount(component);
    const submitButton = wrapper.find('.event-slot').at(0);

    submitButton.simulate('dblclick');

    expect(openDialogModeCreate).toHaveBeenCalled;
  });
});
