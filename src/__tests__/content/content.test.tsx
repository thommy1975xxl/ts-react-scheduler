import React from 'react';
import TestRenderer from 'react-test-renderer';
import { Content } from '../../components/content/Content';

describe('components | content | Content.tsx', () => {
  const component = <Content />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });
});
