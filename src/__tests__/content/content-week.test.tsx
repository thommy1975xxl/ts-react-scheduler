import React from 'react';
import TestRenderer from 'react-test-renderer';
import { ContentWeek } from '../../components/content/ContentWeek';

describe('components | content | ContentWeek.tsx', () => {
  const component = <ContentWeek />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });
});
