import React from 'react';
import TestRenderer from 'react-test-renderer';
import { EventDialogContent } from '../../components/dialog/EventDialogContent';
import { mount } from 'enzyme';

describe('components | dialog | EventDialogHedarer.tsx', () => {
  const component = <EventDialogContent />;

  beforeEach(() => {
    HTMLCanvasElement.prototype.getContext = jest.fn();
  });

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });

  test('Should ensure change for input title', () => {
    const handleChangeTitle = jest.fn();
    const wrapper = mount(component);
    const input = wrapper.find('.event-dialog-title').at(0);

    input.simulate('change', { target: { value: 'Hello' } });

    expect(handleChangeTitle).toHaveBeenCalled;
  });

  test('Should ensure change for input start', () => {
    const handleChangeStart = jest.fn();
    const wrapper = mount(component);
    const input = wrapper.find('.event-dialog-start').at(0);

    input.simulate('change', { target: { value: 'Hello' } });

    expect(handleChangeStart).toHaveBeenCalled;
  });

  test('Should ensure change for input end', () => {
    const handleChangeEnd = jest.fn();
    const wrapper = mount(component);
    const input = wrapper.find('.event-dialog-end').at(0);

    input.simulate('change', { target: { value: 'Hello' } });

    expect(handleChangeEnd).toHaveBeenCalled;
  });

  test('Should ensure change for input description', () => {
    const handleChangeDescription = jest.fn();
    const wrapper = mount(component);
    const input = wrapper.find('.event-dialog-description').at(0);

    input.simulate('change', { target: { value: 'Hello' } });

    expect(handleChangeDescription).toHaveBeenCalled;
  });

  test('Should ensure change for input until', () => {
    const handleChangeUntil = jest.fn();
    const wrapper = mount(component);
    const input = wrapper.find('.event-dialog-until').at(0);

    input.simulate('change', { target: { value: 'Hello' } });

    expect(handleChangeUntil).toHaveBeenCalled;
  });

  test('Should ensure change for input until daily', () => {
    const handleChangeUntilSelection = jest.fn();
    const wrapper = mount(component);
    const input = wrapper.find('.event-dialog-until-checkbox-group').at(0).find('input').at(0);

    input.simulate('change');

    expect(handleChangeUntilSelection).toHaveBeenCalled;
  });

  test('Should ensure change for input until weekly', () => {
    const handleChangeUntilSelection = jest.fn();
    const wrapper = mount(component);
    const input = wrapper.find('.event-dialog-until-checkbox-group').at(0).find('input').at(1);

    input.simulate('change');

    expect(handleChangeUntilSelection).toHaveBeenCalled;
  });

  test('Should ensure change for input until monthly', () => {
    const handleChangeUntilSelection = jest.fn();
    const wrapper = mount(component);
    const input = wrapper.find('.event-dialog-until-checkbox-group').at(0).find('input').at(2);

    input.simulate('change');

    expect(handleChangeUntilSelection).toHaveBeenCalled;
  });

  test('Should ensure change for input until yearly', () => {
    const handleChangeUntilSelection = jest.fn();
    const wrapper = mount(component);
    const input = wrapper.find('.event-dialog-until-checkbox-group').at(0).find('input').at(3);

    input.simulate('change');

    expect(handleChangeUntilSelection).toHaveBeenCalled;
  });
});
