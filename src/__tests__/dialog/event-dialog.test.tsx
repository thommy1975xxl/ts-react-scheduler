import React from 'react';
import TestRenderer from 'react-test-renderer';
import { EventDialog } from '../../components/dialog/EventDialog';

describe('components | dialog | EventDialog.tsx', () => {
  const component = <EventDialog />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });
});
