import React from 'react';
import TestRenderer from 'react-test-renderer';

import { EventDialogHeader } from '../../components/dialog/EventDialogHeader';

describe('components | dialog | EventDialogHedarer.tsx', () => {
  const component = <EventDialogHeader />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });
});
