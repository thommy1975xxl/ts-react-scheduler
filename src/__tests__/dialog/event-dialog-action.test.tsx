import React from 'react';
import TestRenderer from 'react-test-renderer';
import { EventDialogAction } from '../../components/dialog/EventDialogAction';

describe('components | dialog | EventDialogHedarer.tsx', () => {
  const component = <EventDialogAction />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });
});
