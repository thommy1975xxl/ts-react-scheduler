import React from 'react';
import TestRenderer from 'react-test-renderer';

import { HeaderActionDate } from '../../components/header/HeaderActionDate';

describe('components | header | HeaderActionDate.tsx', () => {
  const component = <HeaderActionDate />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });
});
