import React from 'react';
import TestRenderer from 'react-test-renderer';
import { HeaderLongLasting } from '../../components/header/HeaderLongLasting';

describe('components | header | HeaderLongLasting.tsx', () => {
  const component = <HeaderLongLasting />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });
});
