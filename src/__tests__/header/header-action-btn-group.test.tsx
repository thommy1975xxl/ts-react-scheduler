import React from 'react';
import TestRenderer from 'react-test-renderer';
import { mount } from 'enzyme';
import { HeaderActionBtnGroup } from '../../components/header/HeaderActionBtnGroup';

describe('components | header | HeaderActionBtnGroup.tsx', () => {
  const component = <HeaderActionBtnGroup />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });

  test('Should ensure click for day button', () => {
    const handleClickBtnDay = jest.fn();
    const wrapper = mount(component);
    const submitButton = wrapper.find('.header-btn-day').at(0);

    submitButton.simulate('click');

    expect(handleClickBtnDay).toHaveBeenCalled;
  });

  test('Should ensure click for week button', () => {
    const handleClickBtnWeek = jest.fn();
    const wrapper = mount(component);
    const submitButton = wrapper.find('.header-btn-week').at(0);

    submitButton.simulate('click');

    expect(handleClickBtnWeek).toHaveBeenCalled;
  });

  test('Should ensure click for month button', () => {
    const handleClickBtnMonth = jest.fn();
    const wrapper = mount(component);
    const submitButton = wrapper.find('.header-btn-month').at(0);

    submitButton.simulate('click');

    expect(handleClickBtnMonth).toHaveBeenCalled;
  });

  test('Should ensure click for year button', () => {
    const handleClickBtnYear = jest.fn();
    const wrapper = mount(component);
    const submitButton = wrapper.find('.header-btn-year').at(0);

    submitButton.simulate('click');

    expect(handleClickBtnYear).toHaveBeenCalled;
  });
});
