import React from 'react';
import TestRenderer from 'react-test-renderer';
import { Header } from '../../components/header/Header';

describe('components | header | Header.tsx', () => {
  const component = <Header />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });
});
