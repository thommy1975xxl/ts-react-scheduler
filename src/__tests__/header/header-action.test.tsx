import React from 'react';
import TestRenderer from 'react-test-renderer';
import { HeaderAction } from '../../components/header/HeaderAction';

describe('components | header | HeaderAction.tsx', () => {
  const component = <HeaderAction />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });
});
