import React from 'react';
import TestRenderer from 'react-test-renderer';
import { mount } from 'enzyme';
import { HeaderActionNavGroup } from '../../components/header/HeaderActionNavGroup';

describe('components | header | HeaderActionNavGroup.tsx', () => {
  const component = <HeaderActionNavGroup />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });

  test('Should ensure click for nav button left', () => {
    const handleNavLeft = jest.fn();
    const wrapper = mount(component);
    const submitButton = wrapper.find('.header-action-nav-left').at(0);

    submitButton.simulate('click');

    expect(handleNavLeft).toHaveBeenCalled;
  });

  test('Should ensure click for nav button right', () => {
    const handleNavRight = jest.fn();
    const wrapper = mount(component);
    const submitButton = wrapper.find('.header-action-nav-right').at(0);

    submitButton.simulate('click');

    expect(handleNavRight).toHaveBeenCalled;
  });

  test('Should ensure click for nav button today', () => {
    const handleDateNow = jest.fn();
    const wrapper = mount(component);
    const submitButton = wrapper.find('.header-action-nav-today').at(0);

    submitButton.simulate('click');

    expect(handleDateNow).toHaveBeenCalled;
  });
});
