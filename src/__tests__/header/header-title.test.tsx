import React from 'react';
import TestRenderer from 'react-test-renderer';
import { HeaderTitle } from '../../components/header/HeaderTitle';

describe('components | header | HeaderTitle.tsx', () => {
  const component = <HeaderTitle />;

  test('Should render correctly', () => {
    const element = TestRenderer.create(component).toJSON();

    expect(element).toMatchSnapshot();
  });
});
