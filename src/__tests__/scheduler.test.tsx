import React from 'react';
import TestRenderer from 'react-test-renderer';
import { Scheduler } from '../components/Scheduler';

describe('components | Scheduler.tsx', () => {
  it('Should render correctly', () => {
    const tree = TestRenderer.create(<Scheduler />).toJSON();

    expect(tree).toMatchSnapshot();
  });

  test('Should ensure id', () => {
    let testRenderer = TestRenderer.create(<Scheduler />);
    let element: any = testRenderer.toJSON();

    expect(element.props.id).toBe('TsdevReactScheduler');

    testRenderer = TestRenderer.create(<Scheduler id="xyz" />);
    element = testRenderer.toJSON();

    expect(element.props.id).toBe('xyz');
  });

  test('Should ensure class name', () => {
    const testRenderer = TestRenderer.create(<Scheduler subClass="xyz" />);
    const element: any = testRenderer.toJSON();

    expect(element.props.className).toBe('ts-react-scheduler xyz');
  });
});
