import React, { FC, useState, useEffect } from 'react';
import SchedulerContext from '../context/scheduler-context';
import classnames from 'classnames';
import noop from 'lodash/noop';
import './Scheduler.less';
import { getYear, getMonth, getDate } from 'date-fns';
import { ISchedulerProps } from '../interfaces/ISchedulerProps';
import { Header } from './header/Header';
import { Content } from './content/Content';
import { generateDemoEvents } from './../helpers/demo-event.helper';
import { EventItem } from '../entities/EventItem';

export const currentYear = getYear(new Date(Date.now()));
export const currentMonth = getMonth(new Date(Date.now()));
export const currentDay = getDate(new Date(Date.now()));
const demoEvents = generateDemoEvents();

export const Scheduler: FC<ISchedulerProps> = ({
  contentHeight = 300 /* istanbul ignore next */,
  dateMode = 'day' /* istanbul ignore next */,
  dayNameSun = 'Sunday' /* istanbul ignore next */,
  dayNameMon = 'Monday' /* istanbul ignore next */,
  dayNameTue = 'Tuesday' /* istanbul ignore next */,
  dayNameWed = 'Wednesday' /* istanbul ignore next */,
  dayNameThu = 'Thursday' /* istanbul ignore next */,
  dayNameFri = 'Friday' /* istanbul ignore next */,
  dayNameSat = 'Saturday' /* istanbul ignore next */,
  events = demoEvents /* istanbul ignore next */,
  eventDialogTitleCreate = 'Create Event' /* istanbul ignore next */,
  eventDialogTitleUpdate = 'Update Event' /* istanbul ignore next */,
  headerDateFormat = 'YYYY-MM-DD' /* istanbul ignore next */,
  headerTitle = 'Tsdev React Scheduler' /* istanbul ignore next */,
  id = 'TsdevReactScheduler' /* istanbul ignore next */,
  labelHeaderBtnDay = 'Day' /* istanbul ignore next */,
  labelDialogBtnCancel = 'Cancel' /* istanbul ignore next */,
  labelDialogBtnCreate = 'Create' /* istanbul ignore next */,
  labelDialogBtnDelete = 'Delete' /* istanbul ignore next */,
  labelDialogBtnUpdate = 'Update' /* istanbul ignore next */,
  labelDialogColor = 'Color' /* istanbul ignore next */,
  labelDialogDescription = 'Description' /* istanbul ignore next */,
  labelDialogEnd = 'End' /* istanbul ignore next */,
  labelDialogLongLasting = 'Long lasting' /* istanbul ignore next */,
  labelDialogStart = 'Start' /* istanbul ignore next */,
  labelDialogTitle = 'Title' /* istanbul ignore next */,
  labelDialogUntil = 'Until' /* istanbul ignore next */,
  labelDialogUntilDaily = 'Mark as daily event' /* istanbul ignore next */,
  labelDialogUntilWeekly = 'Mark as weekly event' /* istanbul ignore next */,
  labelDialogUntilMonthly = 'Mark as monthly event' /* istanbul ignore next */,
  labelDialogUntilYearly = 'Mark as yearly event' /* istanbul ignore next */,
  labelHeaderBtnMonth = 'Month' /* istanbul ignore next */,
  labelHeaderBtnToday = 'Today' /* istanbul ignore next */,
  labelHeaderBtnWeek = 'Week' /* istanbul ignore next */,
  labelHeaderBtnYear = 'Year' /* istanbul ignore next */,
  monthNameJan = 'January' /* istanbul ignore next */,
  monthNameFeb = 'February' /* istanbul ignore next */,
  monthNameMar = 'March' /* istanbul ignore next */,
  monthNameApr = 'April' /* istanbul ignore next */,
  monthNameMay = 'May' /* istanbul ignore next */,
  monthNameJun = 'June' /* istanbul ignore next */,
  monthNameJul = 'July' /* istanbul ignore next */,
  monthNameAug = 'August' /* istanbul ignore next */,
  monthNameSep = 'September' /* istanbul ignore next */,
  monthNameOct = 'October' /* istanbul ignore next */,
  monthNameNov = 'November' /* istanbul ignore next */,
  monthNameDec = 'December' /* istanbul ignore next */,
  noDataText = 'No data' /* istanbul ignore next */,
  onClickBtnDay = noop /* istanbul ignore next */,
  onClickBtnMonth = noop /* istanbul ignore next */,
  onClickBtnNext = noop /* istanbul ignore next */,
  onClickBtnPrev = noop /* istanbul ignore next */,
  onClickBtnToday = noop /* istanbul ignore next */,
  onClickBtnWeek = noop /* istanbul ignore next */,
  onClickBtnYear = noop /* istanbul ignore next */,
  onCloseEventDialog = noop /* istanbul ignore next */,
  onCreateEvent = noop /* istanbul ignore next */,
  onDeleteEvent = noop /* istanbul ignore next */,
  onOpenEventDialog = noop /* istanbul ignore next */,
  onUpdateEvent = noop /* istanbul ignore next */,
  readOnly = false /* istanbul ignore next */,
  selectedYear = currentYear /* istanbul ignore next */,
  selectedMonth = currentMonth /* istanbul ignore next */,
  selectedDay = currentDay /* istanbul ignore next */,
  showHeader = true /* istanbul ignore next */,
  showHeaderAction = true /* istanbul ignore next */,
  showHeaderBtnGroup = true /* istanbul ignore next */,
  showHeaderBtnGroupDay = true /* istanbul ignore next */,
  showHeaderBtnGroupMonth = true /* istanbul ignore next */,
  showHeaderBtnGroupWeek = true /* istanbul ignore next */,
  showHeaderBtnGroupYear = true /* istanbul ignore next */,
  showHeaderDate = true /* istanbul ignore next */,
  showHeaderLongLasting = true /* istanbul ignore next */,
  showHeaderNavGroup = true /* istanbul ignore next */,
  showHeaderTitle = true /* istanbul ignore next */,
  showNoDataAlert = true /* istanbul ignore next */,
  subClass = '' /* istanbul ignore next */,
  tooltipHeaderBtnNext = 'Next' /* istanbul ignore next */,
  tooltipHeaderBtnPrev = 'Previous' /* istanbul ignore next */,
}) => {
  const [stateEvents, setStateEvents] = useState(events);
  const [stateDateMode, setStateDateMode] = useState(dateMode);
  const [stateSelectedDay, setStateSelectedDay] = useState(selectedDay);
  const [stateSelectedMonth, setStateSelectedMonth] = useState(selectedMonth);
  const [stateSelectedYear, setStateSelectedYear] = useState(selectedYear);
  const [stateEventDialog, setEventDialogState] = useState({
    isOpen: false,
    mode: 'update',
    eventId: '',
    event: new EventItem('', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, false, false, false, false),
  });
  useEffect(() => {
    setStateDateMode(dateMode.toLowerCase());
    setStateEvents(events);
    setStateSelectedDay(selectedDay);
    setStateSelectedMonth(selectedMonth);
    setStateSelectedYear(selectedYear);
  }, [dateMode, events, selectedDay, selectedMonth, selectedYear]);
  const className = classnames('ts-react-scheduler', subClass);
  const inlineStyleHeader = {
    display: showHeader ? 'flex' : /* istanbul ignore next */ 'none',
  };
  return (
    <SchedulerContext.Provider
      value={{
        contentHeight,
        dateMode: stateDateMode,
        dayNameSun,
        dayNameMon,
        dayNameTue,
        dayNameWed,
        dayNameThu,
        dayNameFri,
        dayNameSat,
        events: stateEvents,
        eventDialogState: stateEventDialog,
        eventDialogTitleCreate,
        eventDialogTitleUpdate,
        headerDateFormat,
        headerTitle,
        id,
        labelDialogBtnCancel,
        labelDialogBtnCreate,
        labelDialogBtnDelete,
        labelDialogBtnUpdate,
        labelDialogColor,
        labelDialogDescription,
        labelDialogEnd,
        labelDialogLongLasting,
        labelDialogStart,
        labelDialogTitle,
        labelDialogUntil,
        labelDialogUntilDaily,
        labelDialogUntilWeekly,
        labelDialogUntilMonthly,
        labelDialogUntilYearly,
        labelHeaderBtnDay,
        labelHeaderBtnMonth,
        labelHeaderBtnToday,
        labelHeaderBtnWeek,
        labelHeaderBtnYear,
        monthNameJan,
        monthNameFeb,
        monthNameMar,
        monthNameApr,
        monthNameMay,
        monthNameJun,
        monthNameJul,
        monthNameAug,
        monthNameSep,
        monthNameOct,
        monthNameNov,
        monthNameDec,
        noDataText,
        onClickBtnDay,
        onClickBtnMonth,
        onClickBtnNext,
        onClickBtnPrev,
        onClickBtnToday,
        onClickBtnWeek,
        onClickBtnYear,
        onCloseEventDialog,
        onCreateEvent,
        onDeleteEvent,
        onOpenEventDialog,
        onUpdateEvent,
        readOnly,
        selectedYear: stateSelectedYear,
        selectedMonth: stateSelectedMonth,
        selectedDay: stateSelectedDay,
        setEventDialogState,
        setStateDateMode,
        setStateEvents,
        setStateSelectedDay,
        setStateSelectedMonth,
        setStateSelectedYear,
        showHeader,
        showHeaderAction,
        showHeaderBtnGroup,
        showHeaderBtnGroupDay,
        showHeaderBtnGroupMonth,
        showHeaderBtnGroupWeek,
        showHeaderBtnGroupYear,
        showHeaderDate,
        showHeaderLongLasting,
        showHeaderNavGroup,
        showHeaderTitle,
        showNoDataAlert,
        tooltipHeaderBtnNext,
        tooltipHeaderBtnPrev,
      }}
    >
      <div id={id} className={className}>
        <div className="app-header-wrapper" style={inlineStyleHeader}>
          <Header />
        </div>
        <div className="app-content-wrapper">
          <Content />
        </div>
      </div>
    </SchedulerContext.Provider>
  );
};
