import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 10px;
  color: red;
`;

export interface ITestComponentProps {
  text?: string;
}

export const TestComponent: React.FC<ITestComponentProps> = ({ text }) => (
  <Wrapper>{text ? text : 'Test Component'}</Wrapper>
);
