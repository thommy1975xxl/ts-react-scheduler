import React, { FC, useContext } from 'react';
import SchedulerContext from '../../context/scheduler-context';
import './HeaderAction.less';
import { HeaderActionBtnGroup } from './HeaderActionBtnGroup';
import { HeaderActionDate } from './HeaderActionDate';
import { HeaderActionNavGroup } from './HeaderActionNavGroup';

export const HeaderAction: FC = () => {
  const { showHeaderAction } = useContext(SchedulerContext);
  const inlineStyle = {
    display: showHeaderAction ? 'flex' : /* istanbul ignore next */ 'none',
  };
  return (
    <div className="header-action-component" style={inlineStyle}>
      <HeaderActionBtnGroup />
      <HeaderActionDate />
      <HeaderActionNavGroup />
    </div>
  );
};
