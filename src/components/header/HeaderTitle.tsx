import React, { FC, useContext } from 'react';
import SchedulerContext from '../../context/scheduler-context';
import './HeaderTitle.less';
import Typography from '@material-ui/core/Typography';

export const HeaderTitle: FC = () => {
  const { headerTitle, showHeaderTitle } = useContext(SchedulerContext);
  const inlineStyle = {
    display: showHeaderTitle ? 'flex' : /* istanbul ignore next */ 'none',
  };
  return (
    <div className="header-title-component" style={inlineStyle}>
      <Typography variant="body1">{headerTitle}</Typography>
    </div>
  );
};
