import React, { FC, useContext } from 'react';
import SchedulerContext from '../../context/scheduler-context';
import './HeaderActionDate.less';
import { formatSelectedDate, mapMonthNames } from '../../helpers/date.helper';
import { getWeek } from 'date-fns';
import Typography from '@material-ui/core/Typography';

export const HeaderActionDate: FC = () => {
  const {
    dateMode,
    headerDateFormat,
    labelHeaderBtnWeek,
    monthNameJan,
    monthNameFeb,
    monthNameMar,
    monthNameApr,
    monthNameMay,
    monthNameJun,
    monthNameJul,
    monthNameAug,
    monthNameSep,
    monthNameOct,
    monthNameNov,
    monthNameDec,
    selectedYear,
    selectedMonth,
    selectedDay,
    showHeaderDate,
  } = useContext(SchedulerContext);
  const inlineStyle = {
    display: showHeaderDate ? 'flex' : /* istanbul ignore next */ 'none',
  };
  const renderDate = () => {
    let dateToRender = '';

    const currentWeek = getWeek(new Date(selectedYear, selectedMonth, selectedDay));

    /* istanbul ignore next */
    switch (dateMode) {
      case 'day':
        dateToRender = formatSelectedDate(selectedYear, selectedMonth, selectedDay, headerDateFormat);
        break;
      case 'week':
        dateToRender = `${formatSelectedDate(
          selectedYear,
          selectedMonth,
          selectedDay,
          headerDateFormat,
        )}, ${labelHeaderBtnWeek} ${currentWeek}`;
        break;
      case 'month':
        const monthName = mapMonthNames(
          selectedMonth,
          monthNameJan,
          monthNameFeb,
          monthNameMar,
          monthNameApr,
          monthNameMay,
          monthNameJun,
          monthNameJul,
          monthNameAug,
          monthNameSep,
          monthNameOct,
          monthNameNov,
          monthNameDec,
        );

        dateToRender = `${formatSelectedDate(
          selectedYear,
          selectedMonth,
          selectedDay,
          headerDateFormat,
        )}, ${monthName}`;
        break;
      case 'year':
        dateToRender = formatSelectedDate(selectedYear, selectedMonth, selectedDay, 'YYYY');
        break;
      default:
        dateToRender = '';
    }

    return dateToRender;
  };
  return (
    <div className="header-action-date-component" style={inlineStyle}>
      <Typography variant="body1"> {renderDate()}</Typography>
    </div>
  );
};
