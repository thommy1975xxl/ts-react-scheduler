import React, { FC } from 'react';
import { HeaderTitle } from './HeaderTitle';
import { HeaderAction } from './HeaderAction';
import { HeaderLongLasting } from './HeaderLongLasting';
import './Header.less';

export const Header: FC = () => {
  return (
    <div className="header-component">
      <div className="header-title-wrapper">
        <HeaderTitle />
      </div>
      <div className="header-actions-wrapper">
        <HeaderAction />
      </div>
      <div className="header-long-lasting-wrapper">
        <HeaderLongLasting />
      </div>
    </div>
  );
};
