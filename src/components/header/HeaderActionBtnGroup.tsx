import React, { FC, useContext } from 'react';
import SchedulerContext from '../../context/scheduler-context';
import './HeaderActionBtnGroup.less';
import uuid from 'uuid';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import {
  getEventsForDay,
  getEventsForWeek,
  getEventsForMonth,
  getEventsForYear,
} from './../../helpers/event-filter.helper';
import { getWeek } from 'date-fns';

export const HeaderActionBtnGroup: FC = () => {
  const {
    dateMode,
    events,
    id,
    labelHeaderBtnDay,
    labelHeaderBtnMonth,
    labelHeaderBtnWeek,
    labelHeaderBtnYear,
    onClickBtnDay,
    onClickBtnWeek,
    onClickBtnMonth,
    onClickBtnYear,
    selectedYear,
    selectedMonth,
    selectedDay,
    setStateDateMode,
    showHeaderBtnGroup,
    showHeaderBtnGroupDay,
    showHeaderBtnGroupMonth,
    showHeaderBtnGroupWeek,
    showHeaderBtnGroupYear,
  } = useContext(SchedulerContext);
  const inlineStyle = {
    display: showHeaderBtnGroup ? 'flex' : /* istanbul ignore next */ 'none',
  };
  const handleClickBtnDay = () => {
    const eventsForDay = getEventsForDay(selectedYear, selectedMonth, selectedDay, events);
    setStateDateMode('day');
    onClickBtnDay({
      selectedYear,
      selectedMonth,
      selectedDay,
      eventsForDay: eventsForDay,
    });
  };
  const handleClickBtnMonth = () => {
    const eventsForWeek = getEventsForWeek(selectedYear, selectedMonth, selectedDay, events);
    setStateDateMode('month');
    onClickBtnWeek({
      selectedYear,
      selectedMonth,
      selectedDay,
      eventsForWeek: eventsForWeek,
      week: getWeek(new Date(selectedYear, selectedMonth, selectedDay)),
    });
  };
  const handleClickBtnWeek = () => {
    setStateDateMode('week');
    onClickBtnMonth({
      selectedYear,
      selectedMonth,
      selectedDay,
      eventsForMonth: getEventsForMonth(selectedYear, selectedMonth, events),
    });
  };
  const handleClickBtnYear = () => {
    setStateDateMode('year');
    onClickBtnYear({
      selectedYear,
      selectedMonth,
      selectedDay,
      eventsForYear: getEventsForYear(selectedYear, events),
    });
  };
  const renderBtnGroupItems = () => {
    const buttons = [];

    /* istanbul ignore else */
    if (showHeaderBtnGroupDay) {
      buttons.push(
        <Button
          key={`btn-group-item-${uuid.v4()}`}
          className="header-btn-day"
          id={`${id}_headerBtnDay`}
          size="small"
          variant={dateMode === 'day' ? 'contained' : /* istanbul ignore next */ 'outlined'}
          onClick={handleClickBtnDay}
        >
          {labelHeaderBtnDay}
        </Button>,
      );
    }
    /* istanbul ignore else */
    if (showHeaderBtnGroupWeek) {
      buttons.push(
        <Button
          key={`btn-group-item-${uuid.v4()}`}
          className="header-btn-week"
          id={`${id}_headerBtnWeek`}
          size="small"
          variant={dateMode === 'week' ? /* istanbul ignore next */ 'contained' : 'outlined'}
          onClick={handleClickBtnWeek}
        >
          {labelHeaderBtnWeek}
        </Button>,
      );
    }
    /* istanbul ignore else */
    if (showHeaderBtnGroupMonth) {
      buttons.push(
        <Button
          key={`btn-group-item-${uuid.v4()}`}
          className="header-btn-month"
          id={`${id}_headerBtnMonth`}
          size="small"
          variant={dateMode === 'month' ? /* istanbul ignore next */ 'contained' : 'outlined'}
          onClick={handleClickBtnMonth}
        >
          {labelHeaderBtnMonth}
        </Button>,
      );
    }
    /* istanbul ignore else */
    if (showHeaderBtnGroupYear) {
      buttons.push(
        <Button
          key={`btn-group-item-${uuid.v4()}`}
          className="header-btn-year"
          id={`${id}_headerBtnYear`}
          size="small"
          variant={dateMode === 'year' ? /* istanbul ignore next */ 'contained' : 'outlined'}
          onClick={handleClickBtnYear}
        >
          {labelHeaderBtnYear}
        </Button>,
      );
    }

    return buttons;
  };
  return (
    <div className="header-action-btn-group-component" style={inlineStyle}>
      <ButtonGroup color="primary" aria-label="outlined primary button group">
        {renderBtnGroupItems()}
      </ButtonGroup>
    </div>
  );
};
