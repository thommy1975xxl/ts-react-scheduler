import React, { FC, useContext } from 'react';
import SchedulerContext from '../../context/scheduler-context';
import './HeaderLongLasting.less';
import map from 'lodash/map';
import isEmpty from 'lodash/isEmpty';
import moment from 'moment';
import Alert from '@material-ui/lab/Alert';
import Typography from '@material-ui/core/Typography';
import {
  getLongScaledEventsForDay,
  getLongScaledEventsForWeek,
  getLongScaledEventsForMonth,
  getLongScaledEventsForYear,
  getEventById,
} from './../../helpers/event-filter.helper';
import { IEventItem } from './../../interfaces/IEventItem';

type SchedulerEvent = IEventItem;

export const HeaderLongLasting: FC = () => {
  const {
    dateMode,
    events,
    noDataText,
    selectedYear,
    selectedMonth,
    selectedDay,
    setEventDialogState,
    showHeaderLongLasting,
    showNoDataAlert,
  } = useContext(SchedulerContext);
  let filteredEvents = [] as SchedulerEvent[];
  /* istanbul ignore next */
  switch (dateMode) {
    case 'day':
      filteredEvents = getLongScaledEventsForDay(events, selectedYear, selectedMonth, selectedDay);
      break;
    case 'week':
      filteredEvents = getLongScaledEventsForWeek(events, selectedYear, selectedMonth, selectedDay);
      break;
    case 'month':
      filteredEvents = getLongScaledEventsForMonth(events, selectedYear, selectedMonth, selectedDay);
      break;
    case 'year':
      filteredEvents = getLongScaledEventsForYear(events, selectedYear, selectedMonth, selectedDay);
      break;
    default:
  }
  /* istanbul ignore next */
  const openDialog = (e: any) => {
    const eventId = e.target.closest('.long-scaled-item').getAttribute('data-attribute-event-id');
    setEventDialogState((state: object) => ({
      ...state,
      isOpen: true,
      eventId,
      event: getEventById(eventId, events),
    }));
  };
  const renderEvents = () =>
    map(filteredEvents, (event, index) => {
      const inlineStyleEvent = {
        backgroundColor: event.color,
      };
      return (
        <div
          key={`long-scaled-item-${index}`}
          className="long-scaled-item"
          style={inlineStyleEvent}
          data-attribute-event-id={event.id}
          onClick={openDialog}
        >
          <Typography variant="caption">
            {moment(
              new Date(event.startYear, event.startMonth, event.startDay, event.startHour, event.startMinute),
            ).format('HH:mm')}{' '}
            : {event.title}
          </Typography>
        </div>
      );
    });
  const inlineStyeAlert = {
    display: isEmpty(filteredEvents) && showNoDataAlert ? 'flex' : 'none',
  };
  const inlineStyle = {
    display: showHeaderLongLasting ? 'flex' : /* istanbul ignore next */ 'none',
  };
  return (
    <div className="header-long-lasting-component" style={inlineStyle}>
      <div className="header-long-lasting-items">
        <Alert severity="info" style={inlineStyeAlert}>
          {noDataText}
        </Alert>
        {renderEvents()}
      </div>
    </div>
  );
};
