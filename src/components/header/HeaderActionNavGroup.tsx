import React, { FC, useContext } from 'react';
import SchedulerContext from '../../context/scheduler-context';
import './HeaderActionNavGroup.less';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import NavIconLeft from '@material-ui/icons/ChevronLeft';
import NavIconRight from '@material-ui/icons/ChevronRight';
import Tooltip from '@material-ui/core/Tooltip';
import { getDaysInMonth, getYear, getMonth, getDate, getWeek } from 'date-fns';

export const HeaderActionNavGroup: FC = () => {
  const {
    dateMode,
    id,
    labelHeaderBtnToday,
    onClickBtnNext,
    onClickBtnPrev,
    onClickBtnToday,
    setStateSelectedDay,
    setStateSelectedMonth,
    setStateSelectedYear,
    showHeaderNavGroup,
    tooltipHeaderBtnNext,
    tooltipHeaderBtnPrev,
  } = useContext(SchedulerContext);
  let { selectedYear, selectedMonth, selectedDay } = useContext(SchedulerContext);
  const inlineStyle = {
    display: showHeaderNavGroup ? 'flex' : /* istanbul ignore next */ 'none',
  };
  let currentWeek = getWeek(new Date(selectedYear, selectedMonth, selectedDay));
  const disableNavButtonLeft = dateMode === 'week' && /* istanbul ignore next */ currentWeek === 1;
  const disableNavButtonRight = dateMode === 'week' && /* istanbul ignore next */ currentWeek === 52;
  const daysInMonth = getDaysInMonth(new Date(selectedYear, selectedMonth));
  /* istanbul ignore next */
  const getDateOfWeek = (week: number, year: number) => {
    const calculatedDate = new Date(year, 0, 0 + (week - 1) * 7);

    currentWeek = week;

    return {
      year: getYear(calculatedDate),
      month: getMonth(calculatedDate),
      day: getDate(calculatedDate),
    };
  };
  const handleNavLeft = () => {
    /* istanbul ignore next */
    switch (dateMode) {
      case 'day':
        if (selectedDay === 1) {
          setStateSelectedMonth((selectedMonth -= 1));
          setStateSelectedDay(daysInMonth);
          if (selectedMonth < 0) {
            setStateSelectedMonth(11);
            setStateSelectedYear((selectedYear -= 1));
          }
        } else {
          setStateSelectedDay((selectedDay -= 1));
        }
        onClickBtnPrev({
          selectedYear,
          selectedMonth,
          selectedDay,
        });
        break;
      case 'week':
        const date = new Date(selectedYear, selectedMonth, selectedDay);

        if (currentWeek !== 1) {
          setStateSelectedYear(selectedYear);
          setStateSelectedMonth(getDateOfWeek(getWeek(date) - 1, selectedYear).month);
          setStateSelectedDay(getDateOfWeek(getWeek(date) - 1, selectedYear).day);
        }
        onClickBtnPrev({
          selectedYear,
          selectedMonth,
          selectedDay,
        });
        break;
      case 'month':
        if (selectedMonth === 0) {
          setStateSelectedYear((selectedYear -= 1));
          setStateSelectedMonth(11);
        } else {
          setStateSelectedMonth((selectedMonth -= 1));
        }
        onClickBtnPrev({
          selectedYear,
          selectedMonth,
          selectedDay,
        });
        break;
      case 'year':
        setStateSelectedYear((selectedYear -= 1));
        onClickBtnPrev({
          selectedYear,
          selectedMonth,
          selectedDay,
        });
        break;
      default:
    }
  };
  const handleNavRight = () => {
    /* istanbul ignore next */
    switch (dateMode) {
      case 'day':
        if (selectedDay === daysInMonth) {
          setStateSelectedDay(1);
          if (selectedMonth !== 11) {
            setStateSelectedMonth((selectedMonth += 1));
          } else {
            setStateSelectedMonth(0);
            setStateSelectedYear((selectedYear += 1));
          }
        } else {
          setStateSelectedDay((selectedDay += 1));
        }
        onClickBtnNext({
          selectedYear,
          selectedMonth,
          selectedDay,
        });
        break;
      case 'week':
        const date = new Date(selectedYear, selectedMonth, selectedDay);
        if (currentWeek !== 52) {
          setStateSelectedYear(selectedYear);
          setStateSelectedMonth(getDateOfWeek(getWeek(date) + 1, selectedYear).month);
          setStateSelectedDay(getDateOfWeek(getWeek(date) + 1, selectedYear).day);
        }
        onClickBtnNext({
          selectedYear,
          selectedMonth,
          selectedDay,
        });
        break;
      case 'month':
        if (selectedMonth === 11) {
          setStateSelectedYear((selectedYear += 1));
          setStateSelectedMonth(0);
        } else {
          setStateSelectedMonth((selectedMonth += 1));
        }
        onClickBtnNext({
          selectedYear,
          selectedMonth,
          selectedDay,
        });
        break;
      case 'year':
        setStateSelectedYear((selectedYear += 1));
        onClickBtnNext({
          selectedYear,
          selectedMonth,
          selectedDay,
        });
        break;
      default:
    }
  };
  const handleDateNow = () => {
    setStateSelectedYear(getYear(Date.now()));
    setStateSelectedMonth(getMonth(Date.now()));
    setStateSelectedDay(getDate(Date.now()));
    onClickBtnToday({
      selectedYear: getYear(Date.now()),
      selectedMonth: getMonth(Date.now()),
      selectedDay: getDate(Date.now()),
    });
  };
  /* istanbul ignore if */
  if (disableNavButtonLeft) {
    setStateSelectedDay(1);
    setStateSelectedMonth(0);
  }

  return (
    <div className="header-action-nav-group-component" style={inlineStyle}>
      <Tooltip title={tooltipHeaderBtnPrev}>
        <span>
          <IconButton
            className="header-action-nav-left"
            id={`${id}_headerActionNavLeft`}
            size="small"
            onClick={handleNavLeft}
            disabled={disableNavButtonLeft}
          >
            <NavIconLeft fontSize="small" />
          </IconButton>
        </span>
      </Tooltip>
      <Button
        className="header-action-nav-today"
        id={`${id}_headerActionNavToday`}
        size="small"
        color="primary"
        onClick={handleDateNow}
      >
        {labelHeaderBtnToday}
      </Button>
      <Tooltip title={tooltipHeaderBtnNext}>
        <span>
          <IconButton
            className="header-action-nav-right"
            id={`${id}_headerActionNavRight`}
            size="small"
            onClick={handleNavRight}
            disabled={disableNavButtonRight}
          >
            <NavIconRight fontSize="small" />
          </IconButton>
        </span>
      </Tooltip>
    </div>
  );
};
