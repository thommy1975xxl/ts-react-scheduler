import React, { FC, useContext } from 'react';
import './ContentMonth.less';
import classnames from 'classnames';
import SchedulerContext from './../../context/scheduler-context';
import { getEventsForDay } from './../../helpers/event-filter.helper';
import {
  getDaysInMonth,
  lastDayOfMonth,
  isSunday,
  isMonday,
  isTuesday,
  isWednesday,
  isThursday,
  isFriday,
  isSaturday,
} from 'date-fns';
import { APP_CONST } from '../../appConst';
import Typography from '@material-ui/core/Typography';

export const ContentMonth: FC = () => {
  const { events, selectedDay, selectedMonth, selectedYear, setStateDateMode, setStateSelectedDay } = useContext(
    SchedulerContext,
  );
  const renderGrid = () => {
    const content = [];
    const daysInMonth = [];
    const shiftedDaysIndex = [];
    const firstDayInCurrentMonth = new Date(selectedYear, selectedMonth, 1);
    const firstDayInLastMonth = new Date(selectedYear, selectedMonth - 1, 1);
    const changeDay = (e: any) => {
      e.stopPropagation();
      const newDay = Number(e.target.closest('.month-item').getAttribute('data-attribute-day'));
      setStateSelectedDay(newDay);
      setStateDateMode('day');
    };
    for (let dayCount = 1; dayCount <= getDaysInMonth(new Date(selectedYear, selectedMonth)); dayCount += 1) {
      daysInMonth.push(dayCount);
    }
    // unshift days for right position in calendar
    /* istanbul ignore next */
    if (!isSunday(firstDayInCurrentMonth)) {
      daysInMonth.unshift(getDaysInMonth(lastDayOfMonth(firstDayInLastMonth)));
      shiftedDaysIndex.push(0);
      if (!isMonday(firstDayInCurrentMonth)) {
        daysInMonth.unshift(getDaysInMonth(lastDayOfMonth(firstDayInLastMonth)) - 1);
        shiftedDaysIndex.push(1);
        if (!isTuesday(firstDayInCurrentMonth)) {
          daysInMonth.unshift(getDaysInMonth(lastDayOfMonth(firstDayInLastMonth)) - 2);
          shiftedDaysIndex.push(2);
          if (!isWednesday(firstDayInCurrentMonth)) {
            daysInMonth.unshift(getDaysInMonth(lastDayOfMonth(firstDayInLastMonth)) - 3);
            shiftedDaysIndex.push(3);
            if (!isThursday(firstDayInCurrentMonth)) {
              daysInMonth.unshift(getDaysInMonth(lastDayOfMonth(firstDayInLastMonth)) - 4);
              shiftedDaysIndex.push(4);
              if (!isFriday(firstDayInCurrentMonth)) {
                daysInMonth.unshift(getDaysInMonth(lastDayOfMonth(firstDayInLastMonth)) - 5);
                shiftedDaysIndex.push(5);
                if (!isSaturday(firstDayInCurrentMonth)) {
                  daysInMonth.unshift(getDaysInMonth(lastDayOfMonth(firstDayInLastMonth)) - 6);
                  shiftedDaysIndex.push(6);
                }
              }
            }
          }
        }
      }
    }
    const dayArrayLength = daysInMonth.length;

    // fill day array to 42 items
    /* istanbul ignore else */
    if (dayArrayLength < APP_CONST.totalNumberOfDayItems) {
      const dayDiff = APP_CONST.totalNumberOfDayItems - dayArrayLength;
      for (let day = 1; day <= dayDiff; day += 1) {
        daysInMonth.push(day);
      }
    }
    let monthItemIndex = 1;
    // render matrix
    for (let row = 0; row < APP_CONST.totalNumberOfDayItems / 7; row += 1) {
      const contentRow = [];
      const countDaysInMonth = getDaysInMonth(new Date(selectedYear, selectedMonth, 1));
      for (let column = 0; column < APP_CONST.totalNumberOfDayItems / 6; column += 1) {
        const isDisabled =
          shiftedDaysIndex.indexOf(monthItemIndex - 1) !== -1 ||
          monthItemIndex > countDaysInMonth + shiftedDaysIndex.length;
        const classNameMonthItem = classnames(
          'month-item',
          {
            disabled: isDisabled,
          },
          {
            active:
              daysInMonth[monthItemIndex - 1] === selectedDay /* istanbul ignore next */ &&
              shiftedDaysIndex.indexOf(monthItemIndex - 1) === -1 /* istanbul ignore next */ &&
              monthItemIndex <= countDaysInMonth + shiftedDaysIndex.length,
          },
        );
        const eventCounter = getEventsForDay(selectedYear, selectedMonth, daysInMonth[monthItemIndex - 1], events)
          .length;
        const classNameNumber = classnames(
          eventCounter > 0 && /* istanbul ignore next */ !isDisabled ? /* istanbul ignore next */ 'highlighted' : '',
        );
        contentRow.push(
          <div key={`month-column-${row}-${column}`} className="month-column">
            <div
              className={classNameMonthItem}
              data-attribute-day={daysInMonth[monthItemIndex - 1]}
              onClick={changeDay}
            >
              <div className={classNameNumber}>
                {' '}
                <Typography variant="body2">{daysInMonth[monthItemIndex - 1]}</Typography>
              </div>
            </div>
          </div>,
        );
        monthItemIndex += 1;
      }
      content.push(
        <div key={`month-row-${row}`} className="month-row">
          {contentRow}
        </div>,
      );
    }
    return content;
  };

  return <div className="content-month-component">{renderGrid()}</div>;
};
