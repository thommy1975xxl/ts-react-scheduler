import React, { FC, useContext } from 'react';
import './ContentDay.less';
import uuid from 'uuid';
import moment from 'moment';
import map from 'lodash/map';
import SchedulerContext from './../../context/scheduler-context';
import { getEventsForSlot, getEventById } from './../../helpers/event-filter.helper';
import { APP_CONST } from '../../appConst';
import { createEmptyEvent } from './../../helpers/event.helper';
import Typography from '@material-ui/core/Typography';

export const ContentDay: FC = () => {
  const { events, selectedYear, selectedMonth, selectedDay, setEventDialogState } = useContext(SchedulerContext);
  const renderSlotItems = (start: number, end: number) => {
    const filteredEventsForSlot = getEventsForSlot(events, start, end);
    const itemsToRender = map(filteredEventsForSlot, (item, index) => {
      const topOffset = (item.startMinute / 5) * APP_CONST.fiveMinutesAsPixels;
      const startDate = moment([item.startYear, item.startMonth, item.startDay, item.startHour, item.startMinute]);
      const endDate = moment([item.endYear, item.endMonth, item.endDay, item.endHour, item.endMinute]);
      const timeDiff = endDate.diff(startDate, 'minutes');
      const durationHeight = (timeDiff / 5) * APP_CONST.fiveMinutesAsPixels;
      const inlineStyle = {
        top: topOffset,
        backgroundColor: item.color,
        width: `calc(100% / ${filteredEventsForSlot.length})`,
        height: durationHeight,
      };
      /* istanbul ignore next */
      const openDialog = (e: any) => {
        e.stopPropagation();
        const eventId = e.target.closest('.event-item').getAttribute('data-attribute-event-id');
        setEventDialogState((state: object) => ({
          ...state,
          isOpen: true,
          eventId,
          event: getEventById(eventId, events),
        }));
      };
      return (
        <div
          key={`slot-event-${index}`}
          className="event-item"
          style={inlineStyle}
          data-attribute-event-id={item.id}
          onClick={openDialog}
        >
          <div className="content">
            <Typography variant="caption">
              {`${moment(
                new Date(item.startYear, item.startMonth, item.startDay, item.startHour, item.startMinute),
              ).format('HH:mm')} : ${item.title}`}
            </Typography>
          </div>
        </div>
      );
    });
    return itemsToRender;
  };
  const renderTimeSlots = () => {
    const timeSlotItems = [];
    const openDialogModeCreate = (e: any) => {
      e.stopPropagation();
      const from = Number(e.target.closest('.time-slot').getAttribute('data-attribute-from'));
      const to = Number(e.target.closest('.time-slot').getAttribute('data-attribute-to'));
      const eventId = uuid.v4();
      /* istanbul ignore next */
      setEventDialogState((state: object) /* istanbul ignore next */ => ({
        ...state,
        isOpen: true,
        eventId,
        event: createEmptyEvent(selectedYear, selectedMonth, selectedDay, from, to),
        mode: 'create',
      }));
    };
    for (let timeSlot = 0; timeSlot < 24; timeSlot += 1) {
      timeSlotItems.push(
        <div
          key={`time-slot-${uuid.v4()}`}
          className="time-slot"
          data-attribute-from={timeSlot}
          data-attribute-to={timeSlot + 1}
          onDoubleClick={openDialogModeCreate}
        >
          <Typography variant="caption">{timeSlot < 10 ? `0${timeSlot}:00` : `${timeSlot}:00`}</Typography>
        </div>,
      );
    }
    return timeSlotItems;
  };
  const renderEventSlots = () => {
    const eventSlotItems = [];
    const openDialogModeCreate = (e: any) => {
      e.stopPropagation();
      const from = Number(e.target.closest('.event-slot').getAttribute('data-attribute-from'));
      const to = Number(e.target.closest('.event-slot').getAttribute('data-attribute-to'));
      const eventId = uuid.v4();
      /* istanbul ignore next */
      setEventDialogState((state: object) /* istanbul ignore next */ => ({
        ...state,
        isOpen: true,
        eventId,
        event: createEmptyEvent(selectedYear, selectedMonth, selectedDay, from, to),
        mode: 'create',
      }));
    };
    for (let eventSlot = 0; eventSlot < 24; eventSlot += 1) {
      eventSlotItems.push(
        <div
          key={`event-slot-${uuid.v4()}`}
          className="event-slot"
          data-attribute-from={eventSlot}
          data-attribute-to={eventSlot + 1}
          onDoubleClick={openDialogModeCreate}
        >
          {renderSlotItems(eventSlot, eventSlot + 1)}
        </div>,
      );
    }
    return eventSlotItems;
  };
  return (
    <div className="content-day-component">
      <div className="time-column">{renderTimeSlots()}</div>
      <div id="eventColumn" className="event-column">
        {renderEventSlots()}
      </div>
    </div>
  );
};
