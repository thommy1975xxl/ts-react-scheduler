import React, { FC, useContext } from 'react';
import './ContentScale.less';
import SchedulerContext from './../../context/scheduler-context';
import { mapMonthNames, mapDayNames, formatSelectedDate } from './../../helpers/date.helper';
import { APP_CONST } from '../../appConst';
import Typography from '@material-ui/core/Typography';

export const ContentScale: FC = () => {
  const {
    dateMode,
    dayNameSun,
    dayNameMon,
    dayNameTue,
    dayNameWed,
    dayNameThu,
    dayNameFri,
    dayNameSat,
    monthNameJan,
    monthNameFeb,
    monthNameMar,
    monthNameApr,
    monthNameMay,
    monthNameJun,
    monthNameJul,
    monthNameAug,
    monthNameSep,
    monthNameOct,
    monthNameNov,
    monthNameDec,
    selectedYear,
    selectedMonth,
    selectedDay,
  } = useContext(SchedulerContext);
  const renderScale = () => {
    const selectedDate = new Date(selectedYear, selectedMonth, selectedDay);
    let content = null;
    /* istanbul ignore next */
    switch (dateMode) {
      case 'day':
        const dayName = mapDayNames(
          selectedDate,
          dayNameSun,
          dayNameMon,
          dayNameTue,
          dayNameWed,
          dayNameThu,
          dayNameFri,
          dayNameSat,
        );
        content = (
          <div className="scale-day">
            <Typography variant="body2">{dayName}</Typography>
          </div>
        );
        break;
      case 'week':
        content = (
          <div className="scale-week">
            <div className="scale-week-item">
              <Typography variant="body2">{dayNameSun}</Typography>
            </div>
            <div className="scale-week-item">
              <Typography variant="body2">{dayNameMon}</Typography>
            </div>
            <div className="scale-week-item">
              <Typography variant="body2">{dayNameTue}</Typography>
            </div>
            <div className="scale-week-item">
              <Typography variant="body2">{dayNameWed}</Typography>
            </div>
            <div className="scale-week-item">
              <Typography variant="body2">{dayNameThu}</Typography>
            </div>
            <div className="scale-week-item">
              <Typography variant="body2">{dayNameFri}</Typography>
            </div>
            <div className="scale-week-item">
              <Typography variant="body2">{dayNameSat}</Typography>
            </div>
          </div>
        );
        break;
      case 'month':
        const monthName = mapMonthNames(
          selectedMonth,
          monthNameJan,
          monthNameFeb,
          monthNameMar,
          monthNameApr,
          monthNameMay,
          monthNameJun,
          monthNameJul,
          monthNameAug,
          monthNameSep,
          monthNameOct,
          monthNameNov,
          monthNameDec,
        );
        content = (
          <div className="scale-month">
            <Typography variant="body2">{monthName}</Typography>
          </div>
        );
        break;
      case 'year':
        content = (
          <div className="scale-year">
            <Typography variant="body2">
              {`${formatSelectedDate(
                selectedYear - APP_CONST.yearLeftOffset,
                selectedMonth,
                selectedDay,
                'YYYY',
              )} - ${formatSelectedDate(selectedYear + APP_CONST.yearRightOffset, selectedMonth, selectedDay, 'YYYY')}`}
            </Typography>
          </div>
        );
        break;
      default:
    }
    return content;
  };
  const inlineStyleTimeColumn = {
    display: dateMode === 'day' || dateMode === 'week' ? 'flex' : 'none',
  };
  return (
    <div className="content-scale-component">
      <div className="time-column" style={inlineStyleTimeColumn} />
      <div className="event-column">{renderScale()}</div>
    </div>
  );
};
