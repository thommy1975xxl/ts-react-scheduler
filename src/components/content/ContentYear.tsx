import React, { FC, useContext } from 'react';
import './ContentYear.less';
import SchedulerContext from '../../context/scheduler-context';
import classnames from 'classnames';
import { APP_CONST } from '../../appConst';
import { getEventsForYear } from './../../helpers/event-filter.helper';
import Typography from '@material-ui/core/Typography';

export const ContentYear: FC = () => {
  const { events, selectedYear, setStateSelectedYear, setStateDateMode } = useContext(SchedulerContext);
  const changeYear = (e: any) => {
    e.stopPropagation();

    const newYear = Number(e.target.closest('.year-item').getAttribute('data-attribute-year'));

    setStateSelectedYear(newYear);
    setStateDateMode('month');
  };
  const renderGrid = () => {
    const content = [];
    const startYear = selectedYear - APP_CONST.yearLeftOffset;
    const years = [];

    for (let year = startYear; year <= selectedYear + APP_CONST.yearRightOffset; year += 1) {
      years.push(year);
    }

    let yearItemIndex = 1;

    for (let row = 0; row < APP_CONST.totalNumberOfYearItems / 7; row += 1) {
      const contentRow = [];

      for (let column = 0; column < APP_CONST.totalNumberOfYearItems / 6; column += 1) {
        const classNameYearItem = classnames('year-item', {
          active: years[yearItemIndex - 1] === selectedYear,
        });
        const eventCounter = getEventsForYear(years[yearItemIndex - 1], events).length;
        const classNameNumber = classnames(eventCounter > 0 ? /* istanbul ignore next */ 'highlighted' : '');

        contentRow.push(
          <div key={`year-column-${row}-${column}`} className="year-column">
            <div className={classNameYearItem} onClick={changeYear} data-attribute-year={years[yearItemIndex - 1]}>
              <div className={classNameNumber}>
                <Typography variant="body2">{years[yearItemIndex - 1]}</Typography>
              </div>
            </div>
          </div>,
        );
        yearItemIndex += 1;
      }
      content.push(
        <div key={`year-row-${row}`} className="year-row">
          {contentRow}
        </div>,
      );
    }

    return content;
  };

  return <div className="content-year-component">{renderGrid()}</div>;
};
