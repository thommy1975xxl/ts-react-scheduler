import React, { FC, useContext } from 'react';
import './ContentWeek.less';
import classnames from 'classnames';
import uuid from 'uuid';
import map from 'lodash/map';
import moment from 'moment';
import SchedulerContext from './../../context/scheduler-context';
import { isSunday, isMonday, isTuesday, isWednesday, isThursday, isFriday, isSaturday } from 'date-fns';
import { getEventsForDayName, getEventsForSlot } from './../../helpers/event-filter.helper';
import { EventItem } from '../../entities/EventItem';
import Typography from '@material-ui/core/Typography';

export const ContentWeek: FC = () => {
  const { events, selectedDay, selectedMonth, selectedYear } = useContext(SchedulerContext);
  const selectedDate = new Date(selectedYear, selectedMonth, selectedDay);
  const renderTimeSlots = () => {
    const timeSlotItems = [];
    for (let timeSlot = 0; timeSlot < 24; timeSlot += 1) {
      timeSlotItems.push(
        <div key={`time-slot-${uuid.v4()}`} className="time-slot">
          <Typography variant="caption">{timeSlot < 10 ? `0${timeSlot}:00` : `${timeSlot}:00`}</Typography>
        </div>,
      );
    }
    return timeSlotItems;
  };
  const renderSlotItems = (start: number, end: number, dayIndex: number) => {
    let eventsForSlot: EventItem[] = [];
    /* istanbul ignore next */
    switch (dayIndex) {
      case 0:
        eventsForSlot = getEventsForDayName(events, 'sunday');
        break;
      case 1:
        eventsForSlot = getEventsForDayName(events, 'monday');
        break;
      case 2:
        eventsForSlot = getEventsForDayName(events, 'tuesday');
        break;
      case 3:
        eventsForSlot = getEventsForDayName(events, 'wednesday');
        break;
      case 4:
        eventsForSlot = getEventsForDayName(events, 'thursday');
        break;
      case 5:
        eventsForSlot = getEventsForDayName(events, 'friday');
        break;
      case 6:
        eventsForSlot = getEventsForDayName(events, 'saturday');
        break;
      default:
    }
    const eventsForDay = eventsForSlot;
    const filteredEventsForSlot = getEventsForSlot(eventsForDay, start, end);
    /* istanbul ignore next */
    const itemsToRender = map(filteredEventsForSlot, (item, index) => {
      const inlineStyle = {
        backgroundColor: item.color,
      };
      return (
        <div className="event-item" key={`slot-event-${index}`} style={inlineStyle} data-attribute-event-id={item.id}>
          <div className="content">
            <Typography variant="caption">
              {`${moment(
                new Date(item.startYear, item.startMonth, item.startDay, item.startHour, item.startMinute),
              ).format('HH:mm')} : ${item.title}`}
            </Typography>
          </div>
        </div>
      );
    });
    return itemsToRender;
  };
  const renderEventSlots = () => {
    const eventSlotItems = [];
    let dayIndex = -1;
    /* istanbul ignore next */
    if (isSunday(selectedDate)) {
      dayIndex = 0;
    } else if (isMonday(selectedDate)) {
      dayIndex = 1;
    } else if (isTuesday(selectedDate)) {
      dayIndex = 2;
    } else if (isWednesday(selectedDate)) {
      dayIndex = 3;
    } else if (isThursday(selectedDate)) {
      dayIndex = 4;
    } else if (isFriday(selectedDate)) {
      dayIndex = 5;
    } else if (isSaturday(selectedDate)) {
      dayIndex = 6;
    } else {
      dayIndex = -1;
    }
    for (let eventSlot = 0; eventSlot < 24; eventSlot += 1) {
      const contentRow = [];
      for (let column = 0; column < 7; column += 1) {
        const classNameWeekColumn = classnames('event-slot-item', {
          active: dayIndex === column,
        });
        contentRow.push(
          <div
            key={`week-column-${eventSlot}-${column}`}
            data-attribute-from={eventSlot}
            data-attribute-to={eventSlot + 1}
            className={classNameWeekColumn}
          >
            {renderSlotItems(eventSlot, eventSlot + 1, column)}
          </div>,
        );
      }
      eventSlotItems.push(
        <div key={`event-slot-${uuid.v4()}`} className="event-slot">
          {contentRow}
        </div>,
      );
    }
    return eventSlotItems;
  };

  return (
    <div className="content-week-component">
      <div className="time-column">{renderTimeSlots()}</div>
      <div className="event-column">{renderEventSlots()}</div>
    </div>
  );
};
