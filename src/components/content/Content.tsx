import React, { FC, useContext } from 'react';
import './Content.less';
import SchedulerContext from '../../context/scheduler-context';
import { ContentDay } from './ContentDay';
import { ContentWeek } from './ContentWeek';
import { ContentMonth } from './ContentMonth';
import { ContentYear } from './ContentYear';
import { ContentScale } from './ContentScale';
import { EventDialog } from './../dialog/EventDialog';

export const Content: FC = () => {
  const { contentHeight, dateMode } = useContext(SchedulerContext);
  const inlineStyleContentGrid = {
    height: contentHeight,
  };
  let content = null;
  /* istanbul ignore next */
  switch (dateMode) {
    case 'day':
      content = <ContentDay />;
      break;
    case 'week':
      content = <ContentWeek />;
      break;
    case 'month':
      content = <ContentMonth />;
      break;
    case 'year':
      content = <ContentYear />;
      break;
    default:
  }
  return (
    <div className="content-component">
      <div className="content-scale">
        <ContentScale />
      </div>
      <div className="content-event-grid" style={inlineStyleContentGrid}>
        {content}
      </div>
      <EventDialog />
    </div>
  );
};
