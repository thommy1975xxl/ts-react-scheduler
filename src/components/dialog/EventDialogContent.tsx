import React, { FC, useContext } from 'react';
import './EventDialogContent.less';
import classnames from 'classnames';
import SchedulerContext from './../../context/scheduler-context';
import TextField from '@material-ui/core/TextField';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FormGroup from '@material-ui/core/FormGroup';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { APP_CONST } from './../../appConst';
import { SketchPicker } from 'react-color';
import { mapEventDialogDateTimeInput, mapEventDialogDateInput } from './../../helpers/date.helper';

export const EventDialogContent: FC = () => {
  const {
    id,
    labelDialogColor,
    labelDialogDescription,
    labelDialogEnd,
    labelDialogLongLasting,
    labelDialogStart,
    labelDialogTitle,
    labelDialogUntil,
    labelDialogUntilDaily,
    labelDialogUntilWeekly,
    labelDialogUntilMonthly,
    labelDialogUntilYearly,
    eventDialogState,
    readOnly,
    setEventDialogState,
  } = useContext(SchedulerContext);
  /* istanbul ignore next */
  const eventItem = eventDialogState ? eventDialogState.event : null;
  /* istanbul ignore next */
  const startYear = eventItem ? eventItem.startYear : 0;
  /* istanbul ignore next */
  const startMonth = eventItem ? eventItem.startMonth + 1 : 1;
  /* istanbul ignore next */
  const startDay = eventItem ? eventItem.startDay : 0;
  /* istanbul ignore next */
  const startHour = eventItem ? eventItem.startHour : 0;
  /* istanbul ignore next */
  const startMinute = eventItem ? eventItem.startMinute : 0;
  /* istanbul ignore next */
  const startMonthNormalized = startMonth < 10 ? `0${startMonth}` : startMonth;
  /* istanbul ignore next */
  const startDayNormalized = startDay < 10 ? `0${startDay}` : startDay;
  /* istanbul ignore next */
  const startHourNormalized = startHour < 10 ? `0${startHour}` : startHour;
  /* istanbul ignore next */
  const startMinuteNormalized = startMinute < 10 ? `0${startMinute}` : startMinute;
  const startTimeValue =
    // eslint-disable-next-line max-len
    `${startYear}-${startMonthNormalized}-${startDayNormalized}T${startHourNormalized}:${startMinuteNormalized}`;
  /* istanbul ignore next */
  const endYear = eventItem ? eventItem.endYear : 0;
  /* istanbul ignore next */
  const endMonth = eventItem ? eventItem.endMonth + 1 : 1;
  /* istanbul ignore next */
  const endDay = eventItem ? eventItem.endDay : 0;
  /* istanbul ignore next */
  const endHour = eventItem ? eventItem.endHour : 0;
  /* istanbul ignore next */
  const endMinute = eventItem ? eventItem.endMinute : 0;
  /* istanbul ignore next */
  const endMonthNormalized = endMonth < 10 ? `0${endMonth}` : endMonth;
  /* istanbul ignore next */
  const endDayNormalized = endDay < 10 ? `0${endDay}` : endDay;
  /* istanbul ignore next */
  const endHourNormalized = endHour < 10 ? `0${endHour}` : endHour;
  /* istanbul ignore next */
  const endMinuteNormalized = endMinute < 10 ? `0${endMinute}` : endMinute;
  const endTimeValue = `${endYear}-${endMonthNormalized}-${endDayNormalized}T${endHourNormalized}:${endMinuteNormalized}`;
  /* istanbul ignore next */
  const untilYear = eventItem ? eventItem.untilYear : 0;
  /* istanbul ignore next */
  const untilMonth = eventItem ? eventItem.untilMonth + 1 : 1;
  /* istanbul ignore next */
  const untilDay = eventItem ? eventItem.untilDay : 0;
  /* istanbul ignore next */
  const untilMonthNormalized = untilMonth < 10 ? `0${untilMonth}` : untilMonth;
  /* istanbul ignore next */
  const untilDayNormalized = untilDay < 10 ? `0${untilDay}` : untilDay;
  const untilTimeValue = `${untilYear}-${untilMonthNormalized}-${untilDayNormalized}`;
  const handleChangeTitle = /* istanbul ignore next */ (e: any) => {
    setEventDialogState((state: object) => ({
      ...state,
      event: Object.assign(eventItem, { title: e.target.value }),
    }));
  };
  const handleChangeStart = /* istanbul ignore next */ (e: any) => {
    const { year, month, day, hour, minute } = mapEventDialogDateTimeInput(e.target.value);
    setEventDialogState((state: object) => ({
      ...state,
      event: Object.assign(eventItem, {
        startYear: year,
        startMonth: month,
        startDay: day,
        startHour: hour,
        startMinute: minute,
      }),
    }));
  };
  const handleChangeEnd = /* istanbul ignore next */ (e: any) => {
    const { year, month, day, hour, minute } = mapEventDialogDateTimeInput(e.target.value);
    setEventDialogState((state: object) => ({
      ...state,
      event: Object.assign(eventItem, {
        endYear: year,
        endMonth: month,
        endDay: day,
        endHour: hour,
        endMinute: minute,
      }),
    }));
  };
  const handleChangeDescription = /* istanbul ignore next */ (e: any) => {
    setEventDialogState((state: object) => ({
      ...state,
      event: Object.assign(eventItem, { description: e.target.value }),
    }));
  };
  /* istanbul ignore next */
  const handleChangeColor = (color: any) => {
    setEventDialogState((state: object) => ({
      ...state,
      event: Object.assign(eventItem, { color: color.hex }),
    }));
  };
  const handleChangeUntil = /* istanbul ignore next */ (e: any) => {
    const { year, month, day } = mapEventDialogDateInput(e.target.value);
    setEventDialogState((state: object) => ({
      ...state,
      event: Object.assign(eventItem, {
        untilYear: year,
        untilMonth: month,
        untilDay: day,
      }),
    }));
  };
  const inlineStyleColor = {
    display: readOnly ? /* istanbul ignore next */ 'none' : 'block',
  };
  const classNameReadOnly = classnames(readOnly ? /* istanbul ignore next */ 'read-only' : '');
  const handleChangeDaily = (e: any) => {
    /* istanbul ignore next */
    setEventDialogState((state: object) => ({
      ...state,
      event: Object.assign(eventItem, { isLongLastingDayItem: e.target.checked }),
    }));
  };
  const handleChangeWeekly = (e: any) => {
    /* istanbul ignore next */
    setEventDialogState((state: object) => ({
      ...state,
      event: Object.assign(eventItem, { isLongLastingWeekItem: e.target.checked }),
    }));
  };
  const handleChangeMonthly = (e: any) => {
    /* istanbul ignore next */
    setEventDialogState((state: object) => ({
      ...state,
      event: Object.assign(eventItem, { isLongLastingMonthItem: e.target.checked }),
    }));
  };
  const handleChangeYearly = (e: any) => {
    /* istanbul ignore next */
    setEventDialogState((state: object) => ({
      ...state,
      event: Object.assign(eventItem, { isLongLastingYearItem: e.target.checked }),
    }));
  };
  return (
    <div className="dialog-component-content">
      <TextField
        id={`${id}_eventDialogInputTitle`}
        label={labelDialogTitle}
        className="event-dialog-title"
        size="small"
        value={/* istanbul ignore next */ eventItem ? eventItem.title : ''}
        onChange={handleChangeTitle}
        disabled={readOnly}
      />
      <TextField
        id={`${id}_eventDialogInputStart`}
        className="event-dialog-start"
        size="small"
        label={labelDialogStart}
        type="datetime-local"
        defaultValue={startTimeValue}
        InputLabelProps={{
          shrink: true,
        }}
        onChange={handleChangeStart}
        disabled={readOnly}
      />
      <TextField
        id={`${id}_eventDialogInputEnd`}
        className="event-dialog-end"
        size="small"
        label={labelDialogEnd}
        type="datetime-local"
        defaultValue={endTimeValue}
        InputLabelProps={{
          shrink: true,
        }}
        onChange={handleChangeEnd}
        disabled={readOnly}
      />
      <Accordion>
        <AccordionSummary className={classNameReadOnly} expandIcon={<ExpandMoreIcon />}>
          <Typography>{labelDialogDescription}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <TextField
            id={`${id}_eventDialogInputDescription`}
            className="event-dialog-description"
            size="small"
            multiline
            rows={6}
            variant="outlined"
            value={/* istanbul ignore next */ eventItem ? eventItem.description : ''}
            onChange={handleChangeDescription}
            disabled={readOnly}
          />
        </AccordionDetails>
      </Accordion>
      <Accordion style={inlineStyleColor}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>{labelDialogColor}</Typography>
        </AccordionSummary>
        <AccordionDetails className="content-color-picker">
          <SketchPicker
            color={/* istanbul ignore next */ eventItem ? eventItem.color : APP_CONST.colors[0]}
            presetColors={APP_CONST.colors}
            onChangeComplete={handleChangeColor}
          />
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary className={classNameReadOnly} expandIcon={<ExpandMoreIcon />}>
          <Typography>{labelDialogLongLasting}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <div>
            <TextField
              id={`${id}_eventDialogInputUntil`}
              className="event-dialog-until"
              label={labelDialogUntil}
              type="date"
              defaultValue={untilTimeValue}
              InputLabelProps={{
                shrink: true,
              }}
              fullWidth
              onChange={handleChangeUntil}
              disabled={readOnly}
            />
          </div>
          <FormControl component="fieldset">
            <FormGroup className="event-dialog-until-checkbox-group">
              <FormControlLabel
                control={
                  <Checkbox
                    disabled={readOnly}
                    color="primary"
                    onChange={handleChangeDaily}
                    checked={/* istanbul ignore next */ eventItem ? eventItem.isLongLastingDayItem : false}
                    name="daily"
                  />
                }
                label={labelDialogUntilDaily}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    disabled={readOnly}
                    color="primary"
                    onChange={handleChangeWeekly}
                    checked={/* istanbul ignore next */ eventItem ? eventItem.isLongLastingWeekItem : false}
                    name="weekly"
                  />
                }
                label={labelDialogUntilWeekly}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    disabled={readOnly}
                    color="primary"
                    onChange={handleChangeMonthly}
                    checked={/* istanbul ignore next */ eventItem ? eventItem.isLongLastingMonthItem : false}
                    name="monthly"
                  />
                }
                label={labelDialogUntilMonthly}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    disabled={readOnly}
                    color="primary"
                    onChange={handleChangeYearly}
                    checked={/* istanbul ignore next */ eventItem ? eventItem.isLongLastingYearItem : false}
                    name="yearly"
                  />
                }
                label={labelDialogUntilYearly}
              />
            </FormGroup>
          </FormControl>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};
