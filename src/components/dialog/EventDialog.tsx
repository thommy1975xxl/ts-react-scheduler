import React, { FC, useContext } from 'react';
import SchedulerContext from './../../context/scheduler-context';
import './EventDialog.less';
import Dialog from '@material-ui/core/Dialog';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { EventDialogHeader } from './EventDialogHeader';
import { EventDialogContent } from './EventDialogContent';
import { EventDialogAction } from './EventDialogAction';

export const EventDialog: FC = () => {
  const { eventDialogState, setEventDialogState } = useContext(SchedulerContext);
  /* istanbul ignore next */
  const handleClose = () => {
    setEventDialogState((state: object) => ({
      ...state,
      isOpen: false,
    }));
  };
  return (
    <div data-testid="dialog-component" className="dialog-component">
      <Dialog
        aria-labelledby="simple-dialog-title"
        open={eventDialogState.isOpen}
        onClose={handleClose}
        disableBackdropClick
      >
        <EventDialogHeader />
        <Card className="event-dialog-content-wrapper">
          <CardContent>
            <EventDialogContent />
          </CardContent>
        </Card>
        <div className="event-dialog-action-wrapper">
          <EventDialogAction />
        </div>
      </Dialog>
    </div>
  );
};
