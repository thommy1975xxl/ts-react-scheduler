import React, { FC, useContext } from 'react';
import SchedulerContext from './../../context/scheduler-context';
import './EventDialogHeader.less';
import Typography from '@material-ui/core/Typography';

export const EventDialogHeader: FC = () => {
  const { eventDialogState, eventDialogTitleCreate, eventDialogTitleUpdate } = useContext(SchedulerContext);
  const renderTitle = () => {
    let title = '';
    /* istanbul ignore next */
    switch (eventDialogState?.mode) {
      case 'create':
        title = eventDialogTitleCreate;
        break;
      case 'update':
        title = eventDialogTitleUpdate;
        break;
      default:
        '';
    }
    return title;
  };
  return (
    <div className="dialog-component-header">
      <Typography variant="body1">{renderTitle()}</Typography>
    </div>
  );
};
