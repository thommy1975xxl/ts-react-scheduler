import React, { FC, useContext } from 'react';
import './EventDialogAction.less';
import classnames from 'classnames';
import filter from 'lodash/filter';
import sortBy from 'lodash/sortBy';
import SchedulerContext from './../../context/scheduler-context';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';

export const EventDialogAction: FC = () => {
  const {
    labelDialogBtnCancel,
    labelDialogBtnCreate,
    labelDialogBtnDelete,
    labelDialogBtnUpdate,
    events,
    eventDialogState,
    id,
    onCloseEventDialog,
    onCreateEvent,
    onDeleteEvent,
    onUpdateEvent,
    selectedYear,
    selectedMonth,
    selectedDay,
    setEventDialogState,
    setStateEvents,
  } = useContext(SchedulerContext);
  const className = classnames('event-dialog-action-component');
  /* istanbul ignore next */
  const disableApplyButton = eventDialogState ? eventDialogState.event.title.length === 0 : true;
  /* istanbul ignore next */
  const handleClose = () => {
    setEventDialogState((state: object) => ({
      ...state,
      isOpen: false,
    }));
    onCloseEventDialog({
      selectedYear,
      selectedMonth,
      selectedDay,
    });
  };
  const handleDelete = /* istanbul ignore next */ () => {
    const filteredEvents = filter(events, (event) => event.id !== eventDialogState.eventId);

    setStateEvents(filteredEvents);
    handleClose();
    onDeleteEvent({
      filteredEvents,
      deletedEvent: filter(events, (event) => event.id === eventDialogState.eventId)[0],
    });
  };
  /* istanbul ignore next */
  const handleCancel = () => {
    handleClose();
  };
  /* istanbul ignore next */
  const handleApply = () => {
    if (eventDialogState ? eventDialogState.mode === 'create' : '') {
      setStateEvents(
        sortBy(
          events.concat([eventDialogState.event]),
          'startYear',
          'startMonth',
          'startDay',
          'startHour',
          'startMinute',
        ),
      );
      setEventDialogState((state: object) => ({
        ...state,
        mode: 'update',
      }));
      onCreateEvent({
        newEvent: eventDialogState.event,
      });
    } else {
      const currentEvent = filter(events, (event) => event.id === eventDialogState.eventId)[0];
      const updatedArray = events.map((obj) => (obj.id === eventDialogState.eventId ? currentEvent : obj));

      setStateEvents(updatedArray);
      onUpdateEvent({
        updatedEvent: filter(events, (event) => event.id === eventDialogState.eventId)[0],
      });
    }
    handleClose();
  };
  const renderButtonBar = () => {
    let barContent = null;

    /* istanbul ignore next */
    switch (eventDialogState ? eventDialogState.mode : '') {
      case 'create':
        barContent = (
          <div className="create">
            <ButtonGroup color="primary" aria-label="outlined primary button group">
              <Button
                id={`${id}_dialogBtnCancel`}
                className="dialog-btn-cancel"
                size="small"
                variant="contained"
                color="default"
                onClick={handleCancel}
              >
                {labelDialogBtnCancel}
              </Button>
              <Button
                id={`${id}_dialogBtnUpdate`}
                className="dialog-btn-update"
                size="small"
                variant="contained"
                color="primary"
                onClick={handleApply}
                disabled={disableApplyButton}
              >
                {labelDialogBtnCreate}
              </Button>
            </ButtonGroup>
          </div>
        );
        break;
      case 'update':
        barContent = (
          <div className="update">
            <Button
              id={`${id}_dialogBtnDelete`}
              className="dialog-btn-delete"
              size="small"
              variant="contained"
              color="secondary"
              onClick={handleDelete}
            >
              {labelDialogBtnDelete}
            </Button>
            <ButtonGroup color="primary" aria-label="outlined primary button group">
              <Button
                id={`${id}_dialogBtnCancel`}
                className="dialog-btn-cancel"
                size="small"
                variant="contained"
                color="default"
                onClick={handleCancel}
              >
                {labelDialogBtnCancel}
              </Button>
              <Button
                id={`${id}_dialogBtnUpdate`}
                className="dialog-btn-update"
                size="small"
                variant="contained"
                color="primary"
                onClick={handleApply}
                disabled={disableApplyButton}
              >
                {labelDialogBtnUpdate}
              </Button>
            </ButtonGroup>
          </div>
        );
        break;
      default:
    }
    return barContent;
  };
  return <div className={className}>{renderButtonBar()}</div>;
};
