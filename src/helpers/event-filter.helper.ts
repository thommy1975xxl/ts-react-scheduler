/* eslint-disable require-jsdoc */

import filter from 'lodash/filter';
import sortBy from 'lodash/sortBy';
import {
  getWeek,
  isSunday,
  isMonday,
  isTuesday,
  isWednesday,
  isThursday,
  isFriday,
  isSaturday,
  isEqual,
} from 'date-fns';
import { EventItem } from './../entities/EventItem';

type SchedulerEvent = EventItem;

export function getLongScaledEventsForDay(
  events: SchedulerEvent[],
  selectedYear: number,
  selectedMonth: number,
  selectedDay: number,
): SchedulerEvent[] {
  const selectedDate = new Date(selectedYear, selectedMonth, selectedDay);

  return sortBy(
    filter(events, (event) => {
      const startDate = new Date(event.startYear, event.startMonth, event.startDay);
      const untilDate = new Date(event.untilYear, event.untilMonth, event.untilDay);

      return event.isLongLastingDayItem && selectedDate < untilDate && startDate <= selectedDate;
    }),
    'startYear',
    'startMonth',
    'startDay',
    'startHour',
    'startMinute',
  );
}

export function getLongScaledEventsForWeek(
  events: SchedulerEvent[],
  selectedYear: number,
  selectedMonth: number,
  selectedDay: number,
): SchedulerEvent[] {
  const selectedDate = new Date(selectedYear, selectedMonth, selectedDay);

  return sortBy(
    filter(events, (event) => {
      const startDate = new Date(event.startYear, event.startMonth, event.startDay);
      const untilDate = new Date(event.untilYear, event.untilMonth, event.untilDay);

      return event.isLongLastingWeekItem && selectedDate < untilDate && getWeek(selectedDate) === getWeek(startDate);
    }),
    'startYear',
    'startMonth',
    'startDay',
    'startHour',
    'startMinute',
  );
}

export function getLongScaledEventsForMonth(
  events: SchedulerEvent[],
  selectedYear: number,
  selectedMonth: number,
  selectedDay: number,
): SchedulerEvent[] {
  const selectedDate = new Date(selectedYear, selectedMonth, selectedDay);

  return sortBy(
    filter(events, (event) => {
      const startDate = new Date(event.startYear, event.startMonth, event.startDay);
      const untilDate = new Date(event.untilYear, event.untilMonth, event.untilDay);

      return (
        event.isLongLastingMonthItem &&
        selectedDate < untilDate &&
        startDate <= selectedDate &&
        event.startMonth <= selectedMonth
      );
    }),
    'startYear',
    'startMonth',
    'startDay',
    'startHour',
    'startMinute',
  );
}

export function getLongScaledEventsForYear(
  events: SchedulerEvent[],
  selectedYear: number,
  selectedMonth: number,
  selectedDay: number,
): SchedulerEvent[] {
  const selectedDate = new Date(selectedYear, selectedMonth, selectedDay);

  return sortBy(
    filter(events, (event) => {
      const untilDate = new Date(event.untilYear, event.untilMonth, event.untilDay);

      return event.isLongLastingYearItem && selectedDate < untilDate && event.startYear <= selectedYear;
    }),
    'startYear',
    'startMonth',
    'startDay',
    'startHour',
    'startMinute',
  );
}

export function getEventsForDay(
  selectedYear: number,
  selectedMonth: number,
  selectedDay: number,
  events: SchedulerEvent[],
): SchedulerEvent[] {
  return sortBy(
    filter(events, (event) => {
      const selectedDate = new Date(selectedYear, selectedMonth, selectedDay);
      const eventStartDate = new Date(event.startYear, event.startMonth, event.startDay);

      return isEqual(selectedDate, eventStartDate);
    }),
    'startYear',
    'startMonth',
    'startDay',
    'startHour',
    'startMinute',
  );
}

export function getEventsForWeek(
  selectedYear: number,
  selectedMonth: number,
  selectedDay: number,
  events: SchedulerEvent[],
): SchedulerEvent[] {
  const currentWeek = getWeek(new Date(selectedYear, selectedMonth, selectedDay));

  return sortBy(
    filter(events, (event) => getWeek(new Date(event.startYear, event.startMonth, event.startDay)) === currentWeek),
    'startYear',
    'startMonth',
    'startDay',
    'startHour',
    'startMinute',
  );
}

export function getEventsForMonth(
  selectedYear: number,
  selectedMonth: number,
  events: SchedulerEvent[],
): SchedulerEvent[] {
  return sortBy(
    filter(events, (event) => event.startYear === selectedYear && event.startMonth === selectedMonth),
    'startYear',
    'startMonth',
    'startDay',
    'startHour',
    'startMinute',
  );
}

export function getEventsForYear(selectedYear: number, events: SchedulerEvent[]): SchedulerEvent[] {
  return sortBy(
    filter(events, (event) => event.startYear === selectedYear),
    'startYear',
    'startMonth',
    'startDay',
    'startHour',
    'startMinute',
  );
}

export function getEventById(eventId: string, events: SchedulerEvent[]): SchedulerEvent {
  return filter(events, (event) => event.id === eventId)[0];
}

export function getEventsForDayName(events: SchedulerEvent[], dayName: string): SchedulerEvent[] {
  switch (dayName) {
    case 'sunday':
      return sortBy(
        filter(events, (event) => {
          return isSunday(new Date(event.startYear, event.startMonth, event.startDay));
        }),
        'startYear',
        'startMonth',
        'startDay',
        'startHour',
        'startMinute',
      );
    case 'monday':
      return sortBy(
        filter(events, (event) => isMonday(new Date(event.startYear, event.startMonth, event.startDay))),
        'startYear',
        'startMonth',
        'startDay',
        'startHour',
        'startMinute',
      );
    case 'tuesday':
      return sortBy(
        filter(events, (event) => isTuesday(new Date(event.startYear, event.startMonth, event.startDay))),
        'startYear',
        'startMonth',
        'startDay',
        'startHour',
        'startMinute',
      );
    case 'wednesday':
      return sortBy(
        filter(events, (event) => isWednesday(new Date(event.startYear, event.startMonth, event.startDay))),
        'startYear',
        'startMonth',
        'startDay',
        'startHour',
        'startMinute',
      );
    case 'thursday':
      return sortBy(
        filter(events, (event) => isThursday(new Date(event.startYear, event.startMonth, event.startDay))),
        'startYear',
        'startMonth',
        'startDay',
        'startHour',
        'startMinute',
      );
    case 'friday':
      return sortBy(
        filter(events, (event) => isFriday(new Date(event.startYear, event.startMonth, event.startDay))),
        'startYear',
        'startMonth',
        'startDay',
        'startHour',
        'startMinute',
      );
    case 'saturday':
      return sortBy(
        filter(events, (event) => isSaturday(new Date(event.startYear, event.startMonth, event.startDay))),
        'startYear',
        'startMonth',
        'startDay',
        'startHour',
        'startMinute',
      );
    default:
      return [];
  }
}

export function getEventsForSlot(eventsForDay: SchedulerEvent[], start: number, end: number): SchedulerEvent[] {
  return sortBy(
    filter(eventsForDay, (event) => {
      return start <= event.startHour && event.startHour < end;
    }),
    'startYear',
    'startMonth',
    'startDay',
    'startHour',
    'startMinute',
  );
}
