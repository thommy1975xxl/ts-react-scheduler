import uuid from 'uuid';
import { EventItem } from './../entities/EventItem';
import { APP_CONST } from '../appConst';

export function createEmptyEvent(
  selectedYear: number,
  selectedMonth: number,
  delectedDay: number,
  from: number,
  to: number,
): EventItem {
  return new EventItem(
    uuid.v4(),
    '',
    '',
    APP_CONST.colors[0],
    selectedYear,
    selectedMonth,
    delectedDay,
    from,
    0,
    selectedYear,
    selectedMonth,
    delectedDay,
    to,
    0,
    selectedYear,
    selectedMonth,
    delectedDay,
    false,
    false,
    false,
    false,
  );
}
