import moment from 'moment';
import { isSunday, isMonday, isTuesday, isWednesday, isThursday, isFriday, isSaturday } from 'date-fns';

/* eslint-disable require-jsdoc */
export function formatSelectedDate(
  selectedYear: number,
  selectedMonth: number,
  selectedDay: number,
  headerDateFormat: string,
): string {
  return moment(new Date(selectedYear, selectedMonth, selectedDay)).format(headerDateFormat);
}

export function mapMonthNames(
  selectedMonth: number,
  monthNameJan: string,
  monthNameFeb: string,
  monthNameMar: string,
  monthNameApr: string,
  monthNameMay: string,
  monthNameJun: string,
  monthNameJul: string,
  monthNameAug: string,
  monthNameSep: string,
  monthNameOct: string,
  monthNameNov: string,
  monthNameDec: string,
): string {
  let monthName = '';

  /* istanbul ignore next */
  switch (selectedMonth) {
    case 0:
      monthName = monthNameJan;
      break;
    case 1:
      monthName = monthNameFeb;
      break;
    case 2:
      monthName = monthNameMar;
      break;
    case 3:
      monthName = monthNameApr;
      break;
    case 4:
      monthName = monthNameMay;
      break;
    case 5:
      monthName = monthNameJun;
      break;
    case 6:
      monthName = monthNameJul;
      break;
    case 7:
      monthName = monthNameAug;
      break;
    case 8:
      monthName = monthNameSep;
      break;
    case 9:
      monthName = monthNameOct;
      break;
    case 10:
      monthName = monthNameNov;
      break;
    case 11:
      monthName = monthNameDec;
      break;
    default:
  }

  return monthName;
}

export function mapDayNames(
  selectedDate: Date,
  dayNameSun: string,
  dayNameMon: string,
  dayNameTue: string,
  dayNameWed: string,
  dayNameThu: string,
  dayNameFri: string,
  dayNameSat: string,
): string {
  let dayName = dayNameSun;

  if (isSunday(selectedDate)) {
    dayName = dayNameSun;
  } else if (isMonday(selectedDate)) {
    dayName = dayNameMon;
  } else if (isTuesday(selectedDate)) {
    dayName = dayNameTue;
  } else if (isWednesday(selectedDate)) {
    dayName = dayNameWed;
  } else if (isThursday(selectedDate)) {
    dayName = dayNameThu;
  } else if (isFriday(selectedDate)) {
    dayName = dayNameFri;
  } else if (isSaturday(selectedDate)) {
    dayName = dayNameSat;
  } else {
    dayName = '';
  }

  return dayName;
}

export function getDayNameFromWeekIndex(
  weekIndex: number,
  dayNameSun: string,
  dayNameMon: string,
  dayNameTue: string,
  dayNameWed: string,
  dayNameThu: string,
  dayNameFri: string,
  dayNameSat: string,
) {
  let dayName = '';

  switch (weekIndex) {
    case 0:
      dayName = dayNameSun;
      break;
    case 1:
      dayName = dayNameMon;
      break;
    case 2:
      dayName = dayNameTue;
      break;
    case 3:
      dayName = dayNameWed;
      break;
    case 4:
      dayName = dayNameThu;
      break;
    case 5:
      dayName = dayNameFri;
      break;
    case 6:
      dayName = dayNameSat;
      break;
    /* istanbul ignore next */
    default:
  }

  return dayName;
}

export function mapEventDialogDateTimeInput(input: string) {
  return {
    year: Number(input.substring(0, 4)),
    month: Number(input.substring(5, 7)) - 1,
    day: Number(input.substring(8, 10)),
    hour: Number(input.substring(11, 13)),
    minute: Number(input.substring(14, 16)),
  };
}

export function mapEventDialogDateInput(input: string) {
  return {
    year: Number(input.substring(0, 4)),
    month: Number(input.substring(5, 7)) - 1,
    day: Number(input.substring(8, 10)),
  };
}

export function round5(x: number) {
  return Math.ceil(x / 5) * 5;
}
