/* eslint-disable require-jsdoc */
import { EventItem } from '../entities/EventItem';
import { getYear, getMonth, getDate } from 'date-fns';

const currentYear = getYear(new Date(Date.now()));
const currentMonth = getMonth(new Date(Date.now()));
const currentDay = getDate(new Date(Date.now()));

export function generateDemoEvents() {
  const demoEvents = [];
  const eventA = new EventItem(
    'idA',
    'Demo Event A',
    'This is a description for event A',
    '#64b5f6',
    currentYear,
    currentMonth,
    currentDay,
    3,
    45,
    currentYear,
    currentMonth,
    currentDay,
    4,
    30,
    currentYear + 1,
    currentMonth,
    currentDay,
    true,
    false,
    false,
    false,
  );
  const eventB = new EventItem(
    'idB',
    'Demo Event B',
    'This is a description for event B',
    '#42a5f5',
    currentYear,
    currentMonth,
    currentDay,
    3,
    5,
    currentYear,
    currentMonth,
    currentDay,
    5,
    30,
    currentYear + 1,
    currentMonth,
    currentDay,
    true,
    false,
    false,
    false,
  );
  const eventC = new EventItem(
    'idC',
    'Demo Event C',
    'This is a description for event C',
    '#5c6bc0',
    currentYear,
    currentMonth,
    currentDay,
    3,
    0,
    currentYear,
    currentMonth,
    currentDay,
    5,
    0,
    currentYear + 1,
    currentMonth,
    currentDay,
    true,
    false,
    false,
    false,
  );
  const eventD = new EventItem(
    'idD',
    'Demo Event D',
    'This is a description for event D',
    '#7986cb',
    currentYear,
    currentMonth,
    currentDay,
    3,
    30,
    currentYear,
    currentMonth,
    currentDay,
    5,
    0,
    currentYear + 1,
    currentMonth,
    currentDay,
    true,
    false,
    false,
    false,
  );
  const eventE = new EventItem(
    'idE',
    'Demo Event E',
    'This is a description for event E',
    '#5c6bc0',
    currentYear,
    currentMonth,
    currentDay,
    3,
    35,
    currentYear,
    currentMonth,
    currentDay,
    8,
    50,
    currentYear + 1,
    currentMonth,
    currentDay,
    true,
    false,
    false,
    false,
  );

  demoEvents.push(eventA);
  demoEvents.push(eventB);
  demoEvents.push(eventC);
  demoEvents.push(eventD);
  demoEvents.push(eventE);

  return demoEvents;
}
