/* eslint-disable @typescript-eslint/no-var-requires */
const gulp = require('gulp');
const runSequence = require('run-sequence');

function loadGulpTask(task) {
  return require('./gulp-tasks/' + task)(gulp);
}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PRODUCTION RELATED TASKS
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
gulp.task('prod-compile-less', [], loadGulpTask('prod-compile-less'));

gulp.task('prod', [], function (done) {
  runSequence(['prod-compile-less'], done);
});
